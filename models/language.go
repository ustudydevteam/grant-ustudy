package models

import (
	"grantUstudy/database"
)

type Language struct {
	Id   uint8  `json:"id"`
	Code string `json:"code"`
	Name string `json:"name"`
}

func (l *Language) Get(id uint8) (language Language, err error) {

	statement := "SELECT id, code, name FROM languages WHERE id=$1"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(
		&language.Id,
		&language.Code,
		&language.Name,
	)

	if err != nil {
		return
	}

	return
}

func (l *Language) GetAll() (languages []Language, err error) {

	statement := "SELECT id FROM languages"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	rows, err := stmt.Query()

	for rows.Next() {
		var id uint8
		err := rows.Scan(
			&id,
		)

		if err != nil {
			return languages, err
		}

		language, err := languageModel.Get(id)

		if err != nil {
			return languages, err
		}

		languages = append(languages, language)

	}

	return
}