package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/database"
	"math/rand"
)

type ModuleChunk struct {
	Id          uint32      `json:"id"`
	Module      Module      `json:"module"`
	Questions   []Question  `json:"questions"`
	Composition uint8       `json:"composition"`
	CreatedAt   time.Time   `json:"createdAt"`
	UpdatedAt   time.Time   `json:"updatedAt"`
	DeletedAt   pq.NullTime `json:"-"`
}

func (m *ModuleChunk) Get(id uint32) (moduleChunk ModuleChunk, err error) {
	statement := "SELECT * FROM module_chunks WHERE id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	var moduleId uint32
	err = stmt.QueryRow(id).Scan(
		&moduleChunk.Id,
		&moduleId,
		&moduleChunk.Composition,
		&moduleChunk.CreatedAt,
		&moduleChunk.UpdatedAt,
		&moduleChunk.DeletedAt,
	)

	if err != nil {
		return
	}

	module, err := moduleModel.Get(moduleId)

	if err != nil {
		return
	}

	test, err := module.Test()

	if err != nil {
		return
	}

	languageId := test.Language.Id

	moduleChunk.Module = module

	statement = "SELECT theme_id,question_type_id FROM module_chunk_details WHERE module_chunk_id=$1 AND deleted_at ISNULL"

	stmt, err = database.Db.Prepare(statement)

	var themeId uint32
	var questionTypeId uint8

	err = stmt.QueryRow(moduleChunk.Id).Scan(&themeId, &questionTypeId)

	if err != nil {
		return moduleChunk, err
	}

	questions, err := questionModel.GetByOptions(themeId, questionTypeId, languageId)

	if err != nil {
		return moduleChunk, err
	}

	moduleChunk.Questions = questions

	return
}

func (m *ModuleChunk) ShuffleQuestions() {
	for i := 1; i < len(m.Questions); i++ {
		r := rand.Intn(i + 1)
		if i != r {
			m.Questions[r], m.Questions[i] = m.Questions[i], m.Questions[r]
		}
	}
}
