package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/database"
	"path/filepath"
	"github.com/minio/minio-go"
	"grantUstudy/static"
	"fmt"
)

type Media struct {
	Id           uint32       `json:"id"`
	Name         string      `json:"name"`
	Bucket       Bucket      `json:"bucket"`
	MediableId   uint32       `json:"mediaId"`
	MediableType string      `json:"mediaType"`
	CreatedAt    time.Time   `json:"createdAt"`
	UpdatedAt    time.Time   `json:"updatedAt"`
	DeletedAt    pq.NullTime `json:"-"`
}

func (m *Media) QuestionAudioCreate(questionId uint32, filePath string) (media Media, err error) {

	tx, err := database.Db.Begin()

	if err != nil {
		return
	}

	// to create Question Audio - use bucket with the name = 'audio'
	statement := "SELECT id,name FROM buckets WHERE name='audio' AND deleted_at ISNULL"

	stmt, err := tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return
	}

	var bucketId uint32
	var bucketName string
	err = stmt.QueryRow().Scan(
		&bucketId,
		&bucketName,
	)

	fileName := filepath.Base(filePath)
	fileExt := filepath.Ext(fileName)

	var maxMediaId uint32
	statement = "SELECT max(id) FROM medias"
	err = tx.QueryRow(statement).Scan(&maxMediaId)

	// it should be changed
	if err != nil {
		maxMediaId = 0
	}

	fileName = fmt.Sprint(maxMediaId+1)
	fileName = fileName + fileExt

	statement = "INSERT INTO medias (bucket_id,name,mediable_id,mediable_type) VALUES($1,$2,$3,$4) returning id," +
		"bucket_id,name,mediable_id,mediable_type,created_at,updated_at"

	stmt, err = tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	err = stmt.QueryRow(bucketId, fileName, questionId, "models.Question").Scan(
		&media.Id,
		&bucketId,
		&media.Name,
		&media.MediableId,
		&media.MediableType,
		&media.CreatedAt,
		&media.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	_, err = static.MinioClient.FPutObject(bucketName, fileName, filePath, minio.PutObjectOptions{})

	if err != nil {
		tx.Rollback()
		return
	}

	tx.Commit()

	return
}

func (m *Media) ImagesUpload() {

}

