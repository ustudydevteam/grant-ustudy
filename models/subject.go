package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/database"
	"grantUstudy/forms"
)

type Subject struct {
	Id           uint32               `json:"id"`
	Languages    []Language           `json:"languages"`
	Translations []SubjectTranslation `json:"translations"`
	CreatedAt    time.Time            `json:"createdAt"`
	UpdatedAt    time.Time            `json:"updatedAt"`
	DeletedAt    pq.NullTime          `json:"-"`
}

type SubjectTranslation struct {
	Language Language `json:"language"`
	Name     string   `json:"name"`
}

var languageModel = new(Language)

func (s *Subject) Get(id uint32) (subject Subject, err error) {

	statement := "SELECT id," +
		"created_at, updated_at FROM subjects WHERE id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(
		&subject.Id,
		&subject.CreatedAt,
		&subject.UpdatedAt,
	)

	if err != nil {
		return
	}

	statement = "SELECT language_id, name FROM subject_translations WHERE subject_id=$1"

	stmt, err = database.Db.Prepare(statement)

	if err != nil {
		return
	}

	rows, err := stmt.Query(id)
	defer rows.Close()
	if err != nil {
		return
	}

	translation := SubjectTranslation{}
	for rows.Next() {
		var languageId uint8
		err = rows.Scan(
			&languageId,
			&translation.Name,
		)

		if err != nil {
			return
		}

		language, err := languageModel.Get(languageId)
		if err != nil {
			return subject, err
		}

		translation.Language = language

		subject.Translations = append(subject.Translations, translation)

	}

	statement = "SELECT language_id FROM subject_languages WHERE subject_id=$1"

	stmt, err = database.Db.Prepare(statement)

	if err != nil {
		return
	}

	rows, err = stmt.Query(id)

	for rows.Next() {
		var languageId uint8
		err := rows.Scan(&languageId)

		if err != nil {
			return subject, err
		}

		language, err := languageModel.Get(languageId)

		if err != nil {
			return subject, err
		}

		subject.Languages = append(subject.Languages, language)
	}

	return
}

func (s *Subject) Create(form forms.SubjectCreateForm) (subject Subject, err error) {

	tx, err := database.Db.Begin()

	if err != nil {
		return
	}

	statement := "INSERT INTO subjects DEFAULT VALUES RETURNING " +
		"id, created_at, updated_at"

	stmt, err := tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return
	}

	err = stmt.QueryRow().Scan(
		&subject.Id,
		&subject.CreatedAt,
		&subject.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	for _, val := range form.LanguageIds {
		statement = "INSERT INTO subject_languages (subject_id, language_id) VALUES ($1,$2) RETURNING language_id"

		stmt, err := tx.Prepare(statement)

		if err != nil {
			tx.Rollback()
			return subject, err
		}

		var languageId uint8
		err = stmt.QueryRow(subject.Id, val).Scan(&languageId)

		if err != nil {
			tx.Rollback()
			return subject, err
		}

		language, err := languageModel.Get(languageId)

		if err != nil {
			tx.Rollback()
			return subject, err
		}

		subject.Languages = append(subject.Languages, language)
	}

	for _, val := range form.Translations {

		statement = "INSERT INTO subject_translations (subject_id, language_id, name) VALUES ($1,$2,$3) RETURNING " +
			"language_id, name"

		stmt, err = tx.Prepare(statement)

		if err != nil {
			tx.Rollback()
			return
		}

		var languageId uint8
		var subjectName string
		err = stmt.QueryRow(subject.Id, val.LanguageId, val.Name).Scan(
			&languageId,
			&subjectName,
		)

		if err != nil {
			tx.Rollback()
			return subject, err
		}

		language, err := languageModel.Get(languageId)
		if err != nil {
			tx.Rollback()
			return subject, err
		}

		subject.Translations = append(subject.Translations, SubjectTranslation{
			Language: language,
			Name:     subjectName,
		})

	}
	tx.Commit()
	return
}

func (s *Subject) GetAll() (subjects []Subject, err error) {

	statement := "SELECT id FROM subjects WHERE deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	rows, err := stmt.Query()
	defer rows.Close()
	for rows.Next() {
		var id uint32
		err := rows.Scan(
			&id,
		)

		if err != nil {
			return subjects, err
		}

		subject, err := subjectModel.Get(id)

		if err != nil {
			return subjects, err
		}

		subjects = append(subjects, subject)

	}

	return
}

func (s *Subject) Delete(id uint32) (err error) {

	_, err = subjectModel.Get(id)

	if err != nil {
		return
	}

	tx, err := database.Db.Begin()

	if err != nil {
		tx.Rollback()
		return err
	}

	statement := "UPDATE subjects SET deleted_at = $1 WHERE id = $2"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	_, err = stmt.Exec(time.Now(), id)

	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()

	return
}

func (s *Subject) GetThemes(id uint32) (themes []Theme, err error) {

	statement := "SELECT id FROM themes WHERE subject_id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	rows, err := stmt.Query(id)
	defer rows.Close()
	if err != nil {
		return
	}

	for rows.Next() {
		var id uint32
		err := rows.Scan(
			&id,
		)

		if err != nil {
			return themes, err
		}

		theme, err := themeModel.Get(id)

		if err != nil {
			return themes, err
		}

		themes = append(themes, theme)
	}

	return
}

func (s *Subject) GetQuestionsCount(id uint32) (questionsCount uint32, err error) {

	statement := `SELECT count(DISTINCT questions.id) FROM questions
					INNER JOIN themes ON questions.theme_id = themes.id
					INNER JOIN subjects ON themes.subject_id = subjects.id
					WHERE questions.deleted_at ISNULL AND subjects.deleted_at ISNULL AND themes.deleted_at ISNULL
					AND subjects.id=$1`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(&questionsCount)

	return
}

func (s *Subject) GetModulesCount(id uint32) (modulesCount uint32, err error) {

	statement := `SELECT count(DISTINCT modules.id) FROM modules
					INNER JOIN module_chunks ON modules.id = module_chunks.module_id
					INNER JOIN module_chunk_details ON module_chunks.id = module_chunk_details.module_chunk_id
					INNER JOIN themes ON module_chunk_details.theme_id = themes.id
					INNER JOIN subjects ON themes.subject_id = subjects.id
					WHERE subjects.deleted_at ISNULL AND themes.deleted_at ISNULL AND modules.deleted_at ISNULL
					AND module_chunks.deleted_at ISNULL AND module_chunk_details.deleted_at ISNULL
					AND subjects.id=$1`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(&modulesCount)

	return
}
