package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/forms"
	"database/sql"
)

type Answer struct {
	Id        uint32       `json:"id"`
	Body      string      `json:"body"`
	IsCorrect bool        `json:"-"`
	CreatedAt time.Time   `json:"createdAt"`
	UpdatedAt time.Time   `json:"updatedAt"`
	DeletedAt pq.NullTime `json:"-"`
}

func (a *Answer) Create(form forms.AnswerCreateForm, tx *sql.Tx) (answer Answer, err error) {

	statement := "INSERT INTO answers (question_id,body,is_correct) VALUES ($1,$2,$3) RETURNING " +
		"id,question_id,body,is_correct,created_at,updated_at"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	var questionId uint32
	err = stmt.QueryRow(form.QuestionId, form.Body, form.IsCorrect).Scan(
		&answer.Id,
		&questionId,
		&answer.Body,
		&answer.IsCorrect,
		&answer.CreatedAt,
		&answer.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	return
}

func (a *Answer) Update(form forms.AnswerUpdateForm, tx *sql.Tx) (answer Answer, err error) {

	statement := "UPDATE answers SET  " +
		"question_id = $1, " +
		"body = $2, " +
		"is_correct = $3, " +
		"updated_at = $4 " +
		"WHERE id = $5 RETURNING id,question_id,body,is_correct,created_at,updated_at"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	var questionId uint32
	err = stmt.QueryRow(form.QuestionId, form.Body, form.IsCorrect, time.Now(), form.Id).Scan(
		&answer.Id,
		&questionId,
		&answer.Body,
		&answer.IsCorrect,
		&answer.CreatedAt,
		&answer.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	return
}


