package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/database"
	"grantUstudy/forms"
)

type Theme struct {
	Id           uint32              `json:"id"`
	Subject      Subject            `json:"subject"`
	GradeNumber  uint8              `json:"gradeNumber"`
	Translations []ThemeTranslation `json:"translations"`
	CreatedAt    time.Time          `json:"createdAt"`
	UpdatedAt    time.Time          `json:"updatedAt"`
	DeletedAt    pq.NullTime        `json:"-"`
}

type ThemeTranslation struct {
	Language Language `json:"language"`
	Name     string   `json:"name"`
}

var subjectModel = new(Subject)
var themeModel = new(Theme)

func (t *Theme) Create(form forms.ThemeCreateForm) (theme Theme, err error) {

	tx, err := database.Db.Begin()

	if err != nil {
		return
	}

	statement := "INSERT INTO themes (subject_id, grade_number) VALUES ($1,$2) RETURNING " +
		"id, subject_id, grade_number, created_at, updated_at, deleted_at"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	var subjectId uint32
	err = stmt.QueryRow(form.SubjectId, form.GradeNumber).Scan(
		&theme.Id,
		&subjectId,
		&theme.GradeNumber,
		&theme.CreatedAt,
		&theme.UpdatedAt,
		&theme.DeletedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	subject, err := subjectModel.Get(subjectId)

	if err != nil {
		tx.Rollback()
		return theme, err
	}

	theme.Subject = subject

	for _, val := range form.Translations {

		statement = "INSERT INTO theme_translations (theme_id, language_id, name) VALUES ($1,$2,$3) RETURNING " +
			"language_id, name"

		stmt, err = tx.Prepare(statement)

		if err != nil {
			tx.Rollback()
			return
		}

		var languageId uint8
		var themeName string
		err = stmt.QueryRow(theme.Id, val.LanguageId, val.Name).Scan(
			&languageId,
			&themeName,
		)

		if err != nil {
			tx.Rollback()
			return theme, err
		}

		language, err := languageModel.Get(languageId)
		if err != nil {
			tx.Rollback()
			return theme, err
		}

		theme.Translations = append(theme.Translations, ThemeTranslation{
			Language: language,
			Name:     themeName,
		})

	}

	tx.Commit()

	return
}

func (t *Theme) Get(id uint32) (theme Theme, err error) {

	if err != nil {
		return
	}

	statement := "SELECT * FROM themes WHERE id=$1 AND deleted_at ISNULL"
	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	var subjectId uint32
	err = stmt.QueryRow(id).Scan(
		&theme.Id,
		&subjectId,
		&theme.GradeNumber,
		&theme.CreatedAt,
		&theme.UpdatedAt,
		&theme.DeletedAt,
	)

	if err != nil {
		return
	}

	subject, err := subjectModel.Get(subjectId)

	if err != nil {
		return theme, err
	}

	theme.Subject = subject

	statement = "SELECT language_id, name FROM theme_translations WHERE theme_id=$1"

	stmt, err = database.Db.Prepare(statement)

	if err != nil {
		return
	}

	rows, err := stmt.Query(id)
	defer rows.Close()
	if err != nil {
		return
	}

	translation := ThemeTranslation{}
	for rows.Next() {
		var languageId uint8
		err = rows.Scan(
			&languageId,
			&translation.Name,
		)

		if err != nil {
			return
		}

		language, err := languageModel.Get(languageId)
		if err != nil {
			return theme, err
		}

		translation.Language = language

		theme.Translations = append(theme.Translations, translation)

	}

	return

}

func (t *Theme) Update(id uint32, form forms.ThemeUpdateForm) (theme Theme, err error) {

	tx, err := database.Db.Begin()

	statement := "UPDATE themes SET  " +
		"subject_id = $1, " +
		"grade_number = $2, " +
		"updated_at = $3" +
		"WHERE id = $4 AND deleted_at ISNULL RETURNING id, subject_id, grade_number, created_at, updated_at"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	var subjectId uint32
	err = stmt.QueryRow(form.SubjectId, form.GradeNumber, time.Now(), id).Scan(
		&theme.Id,
		&subjectId,
		&theme.GradeNumber,
		&theme.CreatedAt,
		&theme.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	subject, err := subjectModel.Get(subjectId)

	if err != nil {
		tx.Rollback()
		return theme, err
	}

	theme.Subject = subject

	for _, val := range form.Translations {

		statement = "DELETE FROM theme_translations WHERE theme_id=$1"

		stmt, err = tx.Prepare(statement)

		if err != nil {
			tx.Rollback()
			return
		}

		_, err = stmt.Exec(id)

		if err != nil {
			tx.Rollback()
			return
		}

		statement = "INSERT INTO theme_translations (theme_id, language_id, name) VALUES ($1,$2,$3) RETURNING " +
			"language_id, name"

		stmt, err = tx.Prepare(statement)

		if err != nil {
			tx.Rollback()
			return
		}

		var languageId uint8
		var themeName string
		err = stmt.QueryRow(theme.Id, val.LanguageId, val.Name).Scan(
			&languageId,
			&themeName,
		)

		if err != nil {
			tx.Rollback()
			return theme, err
		}

		language, err := languageModel.Get(languageId)
		if err != nil {
			tx.Rollback()
			return theme, err
		}

		theme.Translations = append(theme.Translations, ThemeTranslation{
			Language: language,
			Name:     themeName,
		})

	}

	tx.Commit()

	return
}

func (t *Theme) Delete(id uint32) (err error) {

	_, err = themeModel.Get(id)

	if err != nil {
		return
	}

	tx, err := database.Db.Begin()

	if err != nil {
		tx.Rollback()
		return err
	}

	statement := "UPDATE themes SET deleted_at = $1 WHERE id = $2"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	_, err = stmt.Exec(time.Now(), id)

	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()

	return
}

func (t *Theme) GetAll() (themes []Theme, err error) {

	statement := "SELECT id FROM themes WHERE deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	rows, err := stmt.Query()
	defer rows.Close()
	for rows.Next() {
		var id uint32
		err := rows.Scan(
			&id,
		)

		if err != nil {
			return themes, err
		}

		theme, err := themeModel.Get(id)

		if err != nil {
			return themes, err
		}

		themes = append(themes, theme)

	}

	return
}

func (t *Theme) GetQuestions(id uint32) (questions []Question, err error) {

	statement := "SELECT id FROM questions WHERE theme_id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	rows, err := stmt.Query(id)
	defer rows.Close()
	if err != nil {
		return
	}

	for rows.Next() {
		var id uint32
		err := rows.Scan(
			&id,
		)

		if err != nil {
			return questions, err
		}

		errCh := make(chan error)
		go func() {
			question, err := questionModel.Get(id)
			errCh <- err
			questions = append(questions, question)
		}()

		err = <-errCh

		if err != nil {
			return questions, err
		}

		close(errCh)
	}

	return
}

func (t *Theme) Filtered(form forms.ThemeFilterForm) (themes []Theme, err error) {

	statement := `SELECT themes.id FROM themes
					INNER JOIN subjects ON themes.subject_id = subjects.id
					WHERE subjects.deleted_at ISNULL AND themes.deleted_at ISNULL
					AND subjects.id=$1 AND grade_number=$2;`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	rows, err := stmt.Query(form.SubjectId, form.GradeNumber)

	if err != nil {
		return
	}

	for rows.Next() {
		var themeId uint32

		err := rows.Scan(
			&themeId,
		)

		if err != nil {
			return themes, err
		}

		theme, err := themeModel.Get(themeId)

		if err != nil {
			return themes, err
		}

		themes = append(themes, theme)
	}

	return
}

func (t *Theme) GetQuestionsCount(id uint32) (questionsCount uint32, err error) {

	statement := "SELECT count(*) FROM questions WHERE theme_id=$1 AND deleted_at ISNULL AND is_approved=TRUE"
	stmt, err := database.Db.Prepare(statement)
	if err != nil {
		return questionsCount, err
	}

	err = stmt.QueryRow(id).Scan(&questionsCount)
	if err != nil {
		return questionsCount, err
	}

	return
}

func (t *Theme) GetModulesCount(id uint32) (modulesCount uint32, err error) {

	statement := `SELECT count(DISTINCT modules.id) FROM modules
					INNER JOIN module_chunks ON modules.id = module_chunks.module_id
					INNER JOIN module_chunk_details ON module_chunks.id = module_chunk_details.module_chunk_id
					INNER JOIN themes ON module_chunk_details.theme_id = themes.id
					WHERE themes.deleted_at ISNULL AND modules.deleted_at ISNULL
					AND module_chunks.deleted_at ISNULL AND module_chunk_details.deleted_at ISNULL
					AND themes.id=$1`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(&modulesCount)

	return
}
