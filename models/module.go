package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/database"
)

type Module struct {
	Id          uint32      `json:"id"`
	IdName      string      `json:"id_name"`
	Name        string      `json:"name"`
	Position    uint8       `json:"position"`
	Description string      `json:"description"`
	CreatedAt   time.Time   `json:"createdAt"`
	UpdatedAt   time.Time   `json:"updatedAt"`
	DeletedAt   pq.NullTime `json:"-"`
}

var moduleChunkModel = new(ModuleChunk)
var testModel = new(Test)

func (m *Module) Get(id uint32) (module Module, err error) {

	statement := "SELECT * FROM modules WHERE id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(
		&module.Id,
		&module.IdName,
		&module.Name,
		&module.Position,
		&module.Description,
		&module.CreatedAt,
		&module.UpdatedAt,
		&module.DeletedAt,
	)

	if err != nil {
		return
	}

	return
}

func (m *Module) Chunks(id uint32) (moduleChunks []ModuleChunk, err error) {

	statement := "SELECT id FROM module_chunks WHERE module_id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	rows, err := stmt.Query(id)

	for rows.Next() {
		var moduleChunkId uint32

		err := rows.Scan(&moduleChunkId)

		if err != nil {
			return moduleChunks, err
		}

		moduleChunk, err := moduleChunkModel.Get(moduleChunkId)

		if err != nil {
			return moduleChunks, err
		}

		moduleChunks = append(moduleChunks, moduleChunk)
	}

	return
}

func (m *Module) Test() (test Test, err error) {

	statement := `SELECT tests.id FROM tests
					INNER JOIN test_modules ON tests.id = test_modules.test_id
					INNER JOIN modules ON test_modules.module_id = modules.id
					WHERE modules.id=$1`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return test, err
	}

	var testId uint32
	err = stmt.QueryRow(m.Id).Scan(&testId)

	if err != nil {
		return test, err
	}

	test, err = testModel.Get(testId)

	if err != nil {
		return test, err
	}

	return
}


