package models

import (
	"time"
	"grantUstudy/forms"
	"grantUstudy/database"
)

type Payment struct {
	Id            uint32          `json:"id"`
	User          User            `json:"user"`
	Provider      PaymentProvider `json:"provider"`
	TransactionId uint32          `json:"transactionId"`
	Amount        float64         `json:"amount"`
	CreatedAt     time.Time       `json:"createdAt"`
}

var providerModel  = new(PaymentProvider)

func (p *Payment) Create(form forms.PaymentCreateForm) (payment Payment, err error) {

	statement := "INSERT INTO payments (user_id, provider_id, transaction_id, amount) RETURNING id, " +
		"user_id, provider_id, transaction_id, amount, created_at"

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	var userId uint32
	var providerId uint32
	err = stmt.QueryRow(form.UserId, form.ProviderId, form.ProviderId, form.TransactionId, form.Amount).Scan(
		&payment.Id, &userId,  &providerId, &payment.TransactionId, &payment.Amount, &payment.CreatedAt,
	)

	if err != nil {
		return
	}

	payment.User, err = userModel.Get(userId)

	if err != nil {
		return
	}

	payment.Provider, err = providerModel.Get(providerId)

	if err != nil {
		return
	}

	return
}
