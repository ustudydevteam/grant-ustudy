package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/forms"
	"grantUstudy/database"
	"database/sql"
	"strings"
	"fmt"
	"encoding/json"
)

type Question struct {
	Id             uint32       `json:"id"`
	ParentQuestion *Question    `json:"parentQuestion"`
	Answers        []Answer     `json:"answers"`
	Theme          Theme        `json:"theme"`
	Type           QuestionType `json:"type"`
	Language       Language     `json:"language"`
	Body           string       `json:"body"`
	IsApproved     bool         `json:"isApproved"`
	CreatedAt      time.Time    `json:"createdAt"`
	UpdatedAt      time.Time    `json:"updatedAt"`
	DeletedAt      pq.NullTime  `json:"-"`
}

var answerModel = new(Answer)
var questionModel = new(Question)
var questionTypeModel = new(QuestionType)

func (q *Question) Create(questionForm forms.QuestionCreateForm, answerForms []forms.AnswerCreateForm) (question Question, err error) {

	tx, err := database.Db.Begin()

	statement := "INSERT INTO questions (" +
		"theme_id," +
		"type_id," +
		"language_id," +
		"body," +
		"is_approved" +
		") VALUES ($1,$2,$3,$4,$5) RETURNING " +
		"id," +
		"theme_id," +
		"type_id," +
		"language_id," +
		"body," +
		"is_approved," +
		"created_at," +
		"updated_at"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	var themeId uint32
	var typeId uint8
	var languageId uint8

	err = stmt.QueryRow(questionForm.ThemeId, questionForm.TypeId,
		questionForm.LanguageId, questionForm.Body, questionForm.IsApproved).Scan(
		&question.Id,
		&themeId,
		&typeId,
		&languageId,
		&question.Body,
		&question.IsApproved,
		&question.CreatedAt,
		&question.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	for _, answerForm := range answerForms {
		answerForm.QuestionId = question.Id
		answer, err := answerModel.Create(answerForm, tx)

		if err != nil {
			tx.Rollback()
			return question, err
		}

		question.Answers = append(question.Answers, answer)

	}

	if questionForm.ParentId != 0 {
		var parentId uint32
		statement = "INSERT INTO parent_questions (parent_id, question_id) VALUES($1,$2) RETURNING parent_id"

		stmt, err = tx.Prepare(statement)

		if err != nil {
			tx.Rollback()
			return question, err
		}

		err = stmt.QueryRow(questionForm.ParentId, question.Id).Scan(&parentId)

		if err != nil {
			tx.Rollback()
			return question, err
		}

	}

	theme, err := themeModel.Get(themeId)

	if err != nil {
		tx.Rollback()
		return question, err
	}

	question.Theme = theme

	language, err := languageModel.Get(languageId)

	if err != nil {
		tx.Rollback()
		return question, err
	}

	question.Language = language

	questionType, err := questionTypeModel.Get(typeId)

	if err != nil {
		tx.Rollback()
		return question, err
	}

	question.Type = questionType

	tx.Commit()
	return
}

func (q *Question) Get(id uint32) (question Question, err error) {

	statement := "SELECT id, theme_id, type_id, language_id, body, is_approved, created_at, " +
		"updated_at, deleted_at FROM questions WHERE id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	var themeId uint32
	var typeId uint8
	var languageId uint8

	err = stmt.QueryRow(id).Scan(
		&question.Id,
		&themeId,
		&typeId,
		&languageId,
		&question.Body,
		&question.IsApproved,
		&question.CreatedAt,
		&question.UpdatedAt,
		&question.DeletedAt,
	)

	if err != nil {
		return question, err
	}

	theme, err := themeModel.Get(themeId)

	if err != nil {
		return
	}

	question.Theme = theme

	questionType, err := questionTypeModel.Get(typeId)

	if err != nil {
		return
	}

	question.Type = questionType

	statement = "SELECT parent_id FROM parent_questions WHERE question_id=$1"
	stmt, err = database.Db.Prepare(statement)

	var parentId uint32
	err = stmt.QueryRow(id).Scan(&parentId)

	if parentId != 0 {
		parentQuestion, err := questionModel.Get(parentId)
		if err != nil {

			return question, err
		}
		question.ParentQuestion = &parentQuestion
	}

	language, err := languageModel.Get(languageId)

	if err != nil {
		return
	}

	question.Language = language

	answers, err := question.GetAnswers()
	if err != nil {
		return
	}
	question.Answers = answers
	return
}

func (q *Question) GetAnswers() (answers []Answer, err error) {

	statement := "SELECT id, body, is_correct, created_at, updated_at FROM answers WHERE question_id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	rows, err := stmt.Query(q.Id)
	defer rows.Close()
	if err != nil {
		return
	}

	for rows.Next() {
		var answer Answer
		err := rows.Scan(
			&answer.Id,
			&answer.Body,
			&answer.IsCorrect,
			&answer.CreatedAt,
			&answer.UpdatedAt,
		)

		if err != nil {
			return answers, err
		}

		answers = append(answers, answer)
	}

	return
}

func (q *Question) Update(id uint32, questionForm forms.QuestionUpdateForm, answerForms []forms.AnswerUpdateForm) (question Question, err error) {

	tx, err := database.Db.Begin()

	statement := "UPDATE questions SET  " +
		"parent_id = $1, " +
		"theme_id = $2, " +
		"type_id = $3, " +
		"language_id = $4, " +
		"body = $5, " +
		"is_approved = $6, " +
		"updated_at = $7 " +
		"WHERE id = $8 RETURNING " +
		"id," +
		"parent_id," +
		"theme_id," +
		"type_id," +
		"language_id," +
		"body," +
		"is_approved," +
		"created_at," +
		"updated_at"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	var parentId uint32
	var themeId uint32
	var typeId uint8
	var languageId uint8

	err = stmt.QueryRow(questionForm.ParentId, questionForm.ThemeId, questionForm.TypeId,
		questionForm.LanguageId, questionForm.Body, questionForm.IsApproved, time.Now(), id).Scan(
		&question.Id,
		&parentId,
		&themeId,
		&typeId,
		&languageId,
		&question.Body,
		&question.IsApproved,
		&question.CreatedAt,
		&question.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	for _, answerForm := range answerForms {
		answerForm.QuestionId = question.Id
		answer, err := answerModel.Update(answerForm, tx)

		if err != nil {
			tx.Rollback()
			return question, err
		}

		question.Answers = append(question.Answers, answer)

	}

	theme, err := themeModel.Get(themeId)

	if err != nil {
		tx.Rollback()
		return question, err
	}

	question.Theme = theme

	language, err := languageModel.Get(languageId)

	if err != nil {
		tx.Rollback()
		return question, err
	}

	question.Language = language

	questionType, err := questionTypeModel.Get(typeId)

	if err != nil {
		tx.Rollback()
		return question, err
	}

	question.Type = questionType

	tx.Commit()
	return
}

func (q *Question) Delete(id uint32) (err error) {

	_, err = questionModel.Get(id)

	if err != nil {
		return
	}

	tx, err := database.Db.Begin()

	if err != nil {
		return
	}

	if err != nil {
		tx.Rollback()
		return err
	}

	statement := "UPDATE questions SET deleted_at = $1 WHERE id = $2"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	_, err = stmt.Exec(time.Now(), id)

	if err != nil {
		tx.Rollback()
		return err
	}

	err = questionModel.DeleteAnswers(id, tx)

	if err != nil {
		tx.Rollback()
		return
	}

	tx.Commit()

	return
}

func (q *Question) DeleteAnswers(id uint32, tx *sql.Tx) (err error) {

	statement := "UPDATE answers SET deleted_at=$1 WHERE question_id=$2"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	_, err = stmt.Query(time.Now(), id)

	if err != nil {
		tx.Rollback()
		return
	}

	return
}

func (q *Question) GetByOptions(themeId uint32, typeId uint8, languageId uint8) (questions []Question, err error) {
	statement := "SELECT id FROM questions WHERE theme_id=$1 AND type_id=$2 AND language_id=$3"

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return questions, err
	}

	rows, err := stmt.Query(themeId, typeId, languageId)

	var questionIds []uint32
	for rows.Next() {
		var questionId uint32
		err := rows.Scan(&questionId)
		if err != nil {
			return questions, err
		}
		questionIds = append(questionIds, questionId)
	}

	questions, err = questionModel.GetByIdSlices(questionIds)
	if err != nil {
		return questions, err
	}
	return
}

func (q *Question) GetByIdSlices(questionIds []uint32) (questions []Question, err error) {

	questionIdsStr := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(questionIds)), ","), "[]")

	statement := fmt.Sprintf(`
				SELECT
				  questions.id,
				  questions.body,
				  question_types.id,
				  question_type_selective_answers.sel_ans_count,
				  question_types.name,
				  question_types.composition,
				  question_types.is_header,
				  question_types.position,
                  get_question_answers(questions.id) questionAnswers
				FROM questions
				  INNER JOIN question_types ON questions.type_id = question_types.id
				  INNER JOIN question_type_selective_answers ON question_types.id = question_type_selective_answers.question_type_id
				WHERE questions.id IN (%v) AND questions.deleted_at ISNULL AND questions.is_approved=TRUE`,
		questionIdsStr)

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return questions, err
	}

	rows, err := stmt.Query()

	if err != nil {
		return questions, err
	}

	for rows.Next() {
		var question Question
		var questionType QuestionType
		var questionTypeSelectiveAnswers QuestionTypeSelectiveAnswers
		var answersStr string
		var answers []Answer
		err := rows.Scan(
			&question.Id,
			&question.Body,
			&questionType.Id,
			&questionTypeSelectiveAnswers.SelectiveAnswersCount,
			&questionType.Name,
			&questionType.Composition,
			&questionType.IsHeader,
			&questionType.Position,
			&answersStr,
		)

		if err != nil {
			return questions, err
		}

		questionType.QuestionTypeSelectiveAnswers = questionTypeSelectiveAnswers
		question.Type = questionType

		err = json.Unmarshal([]byte(answersStr), &answers)

		statement = "SELECT parent_id FROM parent_questions WHERE question_id=$1"
		stmt, err = database.Db.Prepare(statement)

		var parentId uint32
		err = stmt.QueryRow(question.Id).Scan(&parentId)

		if parentId != 0 {
			parentQuestion, err := questionModel.Get(parentId)
			if err != nil {

				return questions, err
			}
			question.ParentQuestion = &parentQuestion
		}

		question.Answers = answers

		questions = append(questions, question)
	}

	return
}

func (q *Question) GetAll() (questions []Question, err error) {

	statement := `SELECT
				  questions.id,
				  questions.body,
				  question_types.id,
				  question_type_selective_answers.sel_ans_count,
				  question_types.name,
				  question_types.composition,
				  question_types.is_header,
				  question_types.position
				FROM questions
				  INNER JOIN question_types ON questions.type_id = question_types.id
				  INNER JOIN question_type_selective_answers ON question_types.id = question_type_selective_answers.question_type_id
				WHERE questions.deleted_at ISNULL AND questions.is_approved=TRUE`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return questions, err
	}

	rows, err := stmt.Query()

	if err != nil {
		return questions, err
	}

	for rows.Next() {
		var question Question
		var questionType QuestionType
		var questionTypeSelectiveAnswers QuestionTypeSelectiveAnswers
		err := rows.Scan(
			&question.Id,
			&question.Body,
			&questionType.Id,
			&questionTypeSelectiveAnswers.SelectiveAnswersCount,
			&questionType.Name,
			&questionType.Composition,
			&questionType.IsHeader,
			&questionType.Position,
		)

		if err != nil {
			return questions, err
		}

		questionType.QuestionTypeSelectiveAnswers = questionTypeSelectiveAnswers
		question.Type = questionType



		questions = append(questions, question)
	}

	return
}
