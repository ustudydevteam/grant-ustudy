package models

import (
	"time"
	"grantUstudy/database"
)

type PaymentProvider struct {
	Id          uint32    `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"createdAt"`
}

func (p *PaymentProvider) Get(id uint32) (paymentProvider PaymentProvider, err error) {

	statement := "SELECT * FROM payment_providers WHERE id=$1"

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(
		&paymentProvider.Id,
		&paymentProvider.Name,
		&paymentProvider.Description,
		&paymentProvider.CreatedAt,
	)

	if err != nil {
		return
	}

	return
}
