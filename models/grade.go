package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/database"
)

type Grade struct {
	Id        uint32       `json:"id"`
	School    School      `json:"school"`
	Number    uint8       `json:"number"`
	Word      string      `json:"word"`
	Language  Language    `json:"language"`
	CreatedAt time.Time   `json:"createdAt"`
	UpdatedAt time.Time   `json:"updatedAt"`
	DeletedAt pq.NullTime `json:"deletedAt"`
}

func (g *Grade) AvailableNumbers() (numbers []int64, err error) {

	statement := "SELECT id FROM grade_numbers"
	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	rows, err := stmt.Query()

	if err != nil {
		return
	}

	for rows.Next() {
		var number int64

		err := rows.Scan(&number)

		if err != nil {
			return numbers, err
		}
		numbers = append(numbers, number)
	}

	return
}
