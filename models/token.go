package models

import (
	"time"
	"github.com/dgrijalva/jwt-go"
	"fmt"
)

type Token struct {
	AccessToken string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
	ExpiresIn int64 `json:"expiresIn"`
}

var AccessSecretKey = "u<6}8IPOb3@T5Hj"
var RefreshSecretKey = "y^({h=bYY3lRl0r"

func (t *Token) Create(user User) (token Token, err error)  {
	jwtAccessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": user.Id,
		"exp":    time.Now().Add(time.Second * 3600).Unix(),
	})

	var secretKey  =  []byte(AccessSecretKey)
	jwtAccessTokenStr, err := jwtAccessToken.SignedString(secretKey)

	if err != nil {
		return
	}

	jwtRefreshTokenStr, err := t.CreateRefreshTokenStr(user)

	if err != nil {
		return
	}

	token.AccessToken = jwtAccessTokenStr
	token.RefreshToken = jwtRefreshTokenStr
	token.ExpiresIn = time.Now().Add(time.Second * 3600).Unix()
	return
}

func (t *Token) CreateRefreshTokenStr(user User) (refreshTokenStr string, err error) {
	jwtRefreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": user.Id,
		"exp":    time.Now().Add(time.Hour * 24).Unix(),
	})

	var secretKey  =  []byte(RefreshSecretKey)
	refreshTokenStr, err = jwtRefreshToken.SignedString(secretKey)
	return
}

func (t *Token) UpdateToken(user User) (token Token, err error) {
	jwtToken, err := jwt.Parse(t.RefreshToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("ErrorWhileParsingToken")
		}
		return []byte(RefreshSecretKey), nil
	})

	if _, ok := jwtToken.Claims.(jwt.MapClaims); ok && jwtToken.Valid {
		token, err = t.Create(user)
	}

	return
}