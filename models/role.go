package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/database"
)

type Role struct {
	Id          uint8       `json:"id"`
	Key         string      `json:"-"`
	Name        string      `json:"name"`
	Description string      `json:"description"`
	CreatedAt   time.Time   `json:"createdAt"`
	UpdatedAt   time.Time   `json:"updatedAt"`
	DeletedAt   pq.NullTime `json:"-"`
}

func (r *Role) Get(id uint8) (role Role, err error) {

	statement := `SELECT roles.*, role_keys.key FROM roles
					INNER JOIN role_keys ON roles.id = role_keys.role_id
					WHERE roles.id=$1 AND deleted_at ISNULL`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(
		&role.Id,
		&role.Name,
		&role.Description,
		&role.CreatedAt,
		&role.UpdatedAt,
		&role.DeletedAt,
		&role.Key,
	)

	if err != nil {
		return
	}

	return
}
