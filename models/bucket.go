package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/forms"
	"grantUstudy/database"
	"grantUstudy/static"
)

type Bucket struct {
	Id          uint32      `json:"id"`
	Name        string      `json:"name"`
	Location    string      `json:"location"`
	Description string      `json:"description"`
	CreatedAt   time.Time   `json:"createdAt"`
	UpdatedAt   time.Time   `json:"updatedAt"`
	DeletedAt   pq.NullTime `json:"deletedAt"`
}

func (b *Bucket) Create(form forms.BucketCreateForm) (bucket Bucket, err error) {

	tx, err := database.Db.Begin()

	if err != nil {
		return
	}

	statement := "INSERT INTO buckets (name,location,description) VALUES ($1,$2,$3) returning id," +
		"name,location,description,created_at,updated_at"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	err = stmt.QueryRow(form.Name, form.Location, form.Description).Scan(
		&bucket.Id,
		&bucket.Name,
		&bucket.Location,
		&bucket.Description,
		&bucket.CreatedAt,
		&bucket.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	err = static.MinioClient.MakeBucket(form.Name, form.Location)

	if err != nil {
		tx.Rollback()
		return
	}

	tx.Commit()

	return
}
