package models

import (
	"grantUstudy/database"
)

type QuestionType struct {
	Id                           uint8                        `json:"id"`
	QuestionTypeSelectiveAnswers QuestionTypeSelectiveAnswers `json:"questionTypeSelectiveAnswers"`
	Name                         string                       `json:"name"`
	Composition                  uint8                        `json:"composition"`
	IsHeader                     bool                         `json:"isHeader"`
	Position                     uint8                        `json:"position"`
}

type QuestionTypeSelectiveAnswers struct {
	SelectiveAnswersCount uint8 `json:"selectiveAnswersCount"`
}

func (t *QuestionType) Get(id uint8) (questionType QuestionType, err error) {

	statement := "SELECT * FROM question_types WHERE id=$1"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	err = stmt.QueryRow(id).Scan(
		&questionType.Id,
		&questionType.Name,
		&questionType.Composition,
		&questionType.IsHeader,
		&questionType.Position,
	)

	if err != nil {
		return
	}

	if !questionType.IsHeader {
		statement = "SELECT sel_ans_count FROM question_type_selective_answers WHERE question_type_id=$1"
		stmt, err = database.Db.Prepare(statement)
		if err != nil {
			return
		}

		var questionTypeSelectiveAnswers QuestionTypeSelectiveAnswers
		err = stmt.QueryRow(id).Scan(&questionTypeSelectiveAnswers.SelectiveAnswersCount)

		if err != nil {
			return
		}

		questionType.QuestionTypeSelectiveAnswers = questionTypeSelectiveAnswers
	}

	return
}

func (t *QuestionType) GetAll() (questionTypes []QuestionType, err error) {

	statement := "SELECT id FROM question_types"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		return
	}

	rows, err := stmt.Query()

	if err != nil {
		return
	}


	for rows.Next() {
		var questionTypeId uint8
		err := rows.Scan(&questionTypeId)
		if err != nil {
			return questionTypes, err
		}

		questionType, err := questionTypeModel.Get(questionTypeId)

		if err != nil {
			return questionTypes, err
		}

		questionTypes = append(questionTypes, questionType)
	}

	return
}


