package models

import (
	"time"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
	"grantUstudy/database"
	"grantUstudy/forms"
	"database/sql"
	"errors"
	"net/http"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"bytes"
	"grantUstudy/env"
	"strings"
	"strconv"
	"grantUstudy/utils"
	"grantUstudy/mail"
)

type User struct {
	Id        uint32      `json:"id"`
	Idn       uint64      `json:"idn"`
	Roles     []Role      `json:"roles"`
	UserInfo  UserInfo    `json:"userInfo"`
	FirstName string      `json:"firstName"`
	LastName  string      `json:"lastName"`
	Email     string      `json:"email"`
	Password  string      `json:"-"`
	CreatedAt time.Time   `json:"createdAt"`
	UpdatedAt time.Time   `json:"updatedAt"`
	DeletedAt pq.NullTime `json:"-"`
}

type UserInfo struct {
	MiddleName     sql.NullString `json:"middleName"`
	BirthDate      pq.NullTime    `json:"birthDate"`
	Gender         sql.NullString `json:"gender"`
	MobilePhone    sql.NullInt64  `json:"mobilePhone"`
	WorkPhone      sql.NullInt64  `json:"workPhone"`
	ExtPhoneNumber sql.NullInt64  `json:"extPhoneNumber"`
	Address        sql.NullString `json:"address"`
}

type UTestRespData struct {
	Success bool   `json:"success"`
	OrderId uint32 `json:"orderId"`
}

type UTestErrMsg struct {
	ErrMessage string `json:"err"`
}

type UTestCompetitionRespData struct {
	CenterId     int       `json:"centerId"`
	LanguageId   int       `json:"languageId"`
	VariantId    int       `json:"variantId"`
	TestDateTime time.Time `json:"testDateTime"`
}

type CompetitionOrder struct {
	CenterId        uint8     `json:"centerId"`
	TestDateTime    time.Time `json:"testDateTime"`
	UStudyVariantId uint32    `json:"uStudyVariantId"`
	LanguageId      uint8     `json:"languageId"`
	CreatedAt       time.Time `json:"createdAt"`
}

type CompetitionList struct {
	User             User             `json:"user"`
	CompetitionOrder CompetitionOrder `json:"competitionOrder"`
}

var roleModel = new(Role)
var userModel = new(User)

func (u *User) Get(id uint32) (user User, err error) {

	statement := `SELECT * FROM users
					INNER JOIN user_info ON users.id = user_info.user_id
					WHERE users.id=$1 AND deleted_at ISNULL`

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()

	if err != nil {
		return
	}

	var userInfoId uint32
	err = stmt.QueryRow(id).Scan(
		&user.Id,
		&user.Idn,
		&user.FirstName,
		&user.LastName,
		&user.Email,
		&user.Password,
		&user.CreatedAt,
		&user.UpdatedAt,
		&user.DeletedAt,
		&userInfoId,
		&user.Id,
		&user.UserInfo.MiddleName,
		&user.UserInfo.BirthDate,
		&user.UserInfo.Gender,
		&user.UserInfo.MobilePhone,
		&user.UserInfo.WorkPhone,
		&user.UserInfo.ExtPhoneNumber,
		&user.UserInfo.Address,
	)

	if err != nil {
		return
	}

	roles, err := user.GetRoles()

	if err != nil {
		return
	}

	user.Roles = roles

	return
}

func (u *User) GetRoles() (roles []Role, err error) {

	statement := `SELECT 
					roles.id, 
					roles.name, 
					roles.description, 
					roles.created_at,
					roles.updated_at
					FROM roles
					INNER JOIN role_user ON roles.id = role_user.role_id
					INNER JOIN users ON role_user.user_id = users.id
					WHERE users.id=$1
					AND users.deleted_at ISNULL 
					AND roles.deleted_at ISNULL`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	res, err := stmt.Query(u.Id)

	if err != nil {
		return
	}

	for res.Next() {
		var id uint8
		var name string
		var description string
		var createdAt time.Time
		var updatedAt time.Time

		err = res.Scan(&id, &name, &description, &createdAt, &updatedAt)

		if err != nil {
			return
		}

		roles = append(roles, Role{
			Id:          id,
			Name:        name,
			Description: description,
			CreatedAt:   createdAt,
			UpdatedAt:   updatedAt,
		})
	}

	return
}

func (u *User) Login(form forms.UserLoginForm) (user User, err error) {

	statement := "SELECT id FROM users WHERE email=LOWER($1)"

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()

	if err != nil {
		return
	}

	var userId uint32
	err = stmt.QueryRow(form.Email).Scan(&userId)

	if err != nil {
		return
	}

	user, err = u.Get(userId)

	if err != nil {
		return
	}

	bytePassword := []byte(form.Password)
	byteHashedPassword := []byte(user.Password)

	err = bcrypt.CompareHashAndPassword(byteHashedPassword, bytePassword)

	if err != nil {
		return
	}

	return
}

func (u *User) Create(form forms.UserCreateForm) (user User, err error) {

	tx, err := database.Db.Begin()

	role, err := roleModel.Get(form.Role.Id)

	if err != nil {
		tx.Rollback()
		return user, err
	}

	if role.Key != form.Role.Key {
		err = errors.New("role keys does not match")
		tx.Rollback()
		return user, err
	}

	statement := "INSERT INTO users (idn, first_name, last_name, email, password) VALUES ($1,$2,$3,$4,$5) RETURNING id"

	stmt, err := tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return
	}

	bytePassword := []byte(form.Password)
	hashedPassword, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)

	if err != nil {
		tx.Rollback()
		return
	}

	var userId uint32
	err = stmt.QueryRow(form.Idn, form.FirstName, form.LastName, form.Email, hashedPassword).Scan(&userId)

	if err != nil {
		tx.Rollback()
		return
	}

	statement = "INSERT INTO role_user (user_id, role_id) VALUES ($1,$2) RETURNING id"

	stmt, err = tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return
	}

	var roleUserId uint32
	err = stmt.QueryRow(userId, form.Role.Id).Scan(&roleUserId)

	if err != nil {
		tx.Rollback()
		return
	}

	tx.Commit()

	statement = "UPDATE user_info SET mobile_phone=$1 WHERE user_id=$2"
	stmt, err = database.Db.Prepare(statement)

	if err != nil {
		return
	}

	_, err = stmt.Exec(form.MobilePhone, userId)

	if err != nil {
		return
	}

	user, err = userModel.Get(userId)

	if err != nil {
		return
	}

	userIdnStr := strconv.Itoa(int(user.Idn))
	s := []string{env.UtestApi, "grant/register/", userIdnStr}
	apiStr := strings.Join(s, "")

	resp, err := http.Get(apiStr)

	if err != nil {
		return user, err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		_, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return user, err
		}

	} else {
		err = errors.New("error encountered when connecting to UTest API server")
		return user, err
	}

	return
}

func (u *User) GetByIdn(idn uint64) (user User, err error) {

	statement := `SELECT * FROM users
					INNER JOIN user_info ON users.id = user_info.user_id
					WHERE users.idn=$1 AND deleted_at ISNULL`

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()

	if err != nil {
		return
	}

	var userInfoId uint32
	err = stmt.QueryRow(idn).Scan(
		&user.Id,
		&user.Idn,
		&user.FirstName,
		&user.LastName,
		&user.Email,
		&user.Password,
		&user.CreatedAt,
		&user.UpdatedAt,
		&user.DeletedAt,
		&userInfoId,
		&user.Id,
		&user.UserInfo.MiddleName,
		&user.UserInfo.BirthDate,
		&user.UserInfo.Gender,
		&user.UserInfo.MobilePhone,
		&user.UserInfo.WorkPhone,
		&user.UserInfo.ExtPhoneNumber,
		&user.UserInfo.Address,
	)

	if err != nil {
		return
	}

	roles, err := user.GetRoles()

	if err != nil {
		return
	}

	user.Roles = roles

	return
}

func (u *User) Pay(amount float64, tx *sql.Tx) (paymentId uint32, err error) {

	statement := "INSERT INTO user_payments (user_id, amount) VALUES ($1,$2) RETURNING id"

	stmt, err := tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return paymentId, err
	}

	err = stmt.QueryRow(u.Id, amount).Scan(
		&paymentId,
	)

	if err != nil {
		tx.Rollback()
		return paymentId, err
	}

	return
}

func (u *User) OrderTest(test Test, selectedModuleIds []uint32) (orderId uint32, generatedTest GeneratedTest, err error) {

	tx, err := database.Db.Begin()
	var generatedModules []GeneratedModule
	if err != nil {
		tx.Rollback()
		return
	}

	userPaymentId, err := u.Pay(test.TestInfo.Price, tx)

	if err != nil {
		tx.Rollback()
		return
	}

	statement := "INSERT INTO orders (user_payment_id, test_id) 	VALUES ($1,$2) RETURNING id"

	stmt, err := tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return
	}

	err = stmt.QueryRow(userPaymentId, test.Id).Scan(
		&orderId,
	)

	if err != nil {
		tx.Rollback()
		return
	}

	selectedModules, err := test.SelectedModules(selectedModuleIds)

	if err != nil {
		return orderId, generatedTest, err
	}

	if int(test.TestInfo.SelectiveModulesCount) != len(selectedModules) {
		err = errors.New("please, select modules of the test")
		return orderId, generatedTest, err
	}

	test.Modules = append(test.Modules, selectedModules...)

	for _, module := range test.Modules {
		moduleChunks, err := module.Chunks(module.Id)
		if err != nil {
			tx.Rollback()
			return orderId, generatedTest, err
		}
		var generatedQuestions []GeneratedQuestion
		for _, moduleChunk := range moduleChunks {
			moduleChunk.ShuffleQuestions()
			moduleChunkQuestions := moduleChunk.Questions[0:moduleChunk.Composition]
			for _, moduleChunkQuestion := range moduleChunkQuestions {
				statement := "INSERT INTO user_variants (order_id, question_id, module_id) VALUES ($1,$2,$3)RETURNING id"
				stmt, err := tx.Prepare(statement)

				if err != nil {
					tx.Rollback()
					return orderId, generatedTest, err
				}

				var userVariantId uint32
				err = stmt.QueryRow(orderId, moduleChunkQuestion.Id, module.Id).Scan(&userVariantId)

				if err != nil {
					tx.Rollback()
					return orderId, generatedTest, err
				}
				var generatedQuestion GeneratedQuestion
				generatedQuestion.UserVariantId = userVariantId
				generatedQuestion.Question = moduleChunkQuestion
				generatedQuestions = append(generatedQuestions, generatedQuestion)
			}
		}

		generatedModule := GeneratedModule{
			Id:        module.Id,
			Name:      module.Name,
			Position:  module.Position,
			Questions: generatedQuestions,
		}

		generatedModules = append(generatedModules, generatedModule)
	}

	generatedTest.Modules = generatedModules
	generatedTest.Test = test

	tx.Commit()
	return
}

func (u *User) OrderTrialTestByUtest(token string, test Test, selectedModuleIds []uint32) (orderId uint32, generatedTest GeneratedTest, err error) {
	uStudyProviderId := env.UStudyProviderId
	idn := fmt.Sprint(u.Idn)
	s := []string{env.UtestApi, "grant/orderTrialTest/", idn, "/", token}
	apiStr := strings.Join(s, "")

	resp, err := http.Get(apiStr)

	if err != nil {
		return orderId, generatedTest, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return orderId, generatedTest, err
	}

	if resp.StatusCode == http.StatusOK {

		var respData UTestRespData
		err = json.Unmarshal(body, &respData)

		if err != nil {
			return orderId, generatedTest, err
		}

		statement := "INSERT INTO payments (user_id, provider_id, transaction_id, amount) VALUES ($1,$2,$3,$4) RETURNING id"
		stmt, err := database.Db.Prepare(statement)

		if err != nil {
			return orderId, generatedTest, err
		}
		var paymentId uint32
		err = stmt.QueryRow(u.Id, uStudyProviderId, respData.OrderId, test.TestInfo.Price).Scan(&paymentId)

		if err != nil {
			return orderId, generatedTest, err
		}

		orderId, generatedTest, err = u.OrderTest(test, selectedModuleIds)

	} else {
		var uTestErrMsg UTestErrMsg
		err = json.Unmarshal(body, &uTestErrMsg)

		if err != nil {
			return orderId, generatedTest, err
		}

		err = errors.New(uTestErrMsg.ErrMessage)

		return orderId, generatedTest, err
	}

	return

}

func (u *User) Orders() (orders []Order, err error) {
	statement := `SELECT orders.id, orders.created_at FROM orders
					INNER JOIN user_payments ON orders.user_payment_id = user_payments.id
					WHERE user_payments.user_id=$1`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return orders, err
	}

	rows, err := stmt.Query(u.Id)

	if err != nil {
		return orders, err
	}

	for rows.Next() {
		var order Order
		err := rows.Scan(
			&order.Id,
			&order.CreatedAt,
		)

		if err != nil {
			return orders, err
		}

		orders = append(orders, order)
	}

	return
}

func (u *User) IsAdmin() (bool) {

	statement := `SELECT roles.name FROM users
					INNER JOIN role_user ON users.id = role_user.user_id
					INNER JOIN roles ON role_user.role_id = roles.id
					WHERE users.id=$1`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return false
	}

	rows, err := stmt.Query(u.Id)

	if err != nil {
		return false
	}

	for rows.Next() {
		var userRole string
		err = rows.Scan(&userRole)

		if err != nil {
			return false
		}

		if userRole == "admin" {
			return true
		}
	}

	return false
}

func (u *User) Compete(form forms.UserCompeteForm) (err error) {

	statement := "SELECT count(*) FROM competition_lists WHERE user_id=$1"
	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	var userCompListCount uint8
	err = stmt.QueryRow(u.Id).Scan(&userCompListCount)

	if err != nil {
		return
	}

	if userCompListCount > 0 {
		err = errors.New("the user already participates in the competition")
		return
	}

	s := []string{env.UtestApi, "grant/compete"}
	apiStr := strings.Join(s, "")

	sendData, err := json.Marshal(form)

	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", apiStr, bytes.NewBuffer(sendData))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	if resp.StatusCode == http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		var uTestCompetitionRespData UTestCompetitionRespData
		err = json.Unmarshal(body, &uTestCompetitionRespData)

		if err != nil {
			return err
		}

		statement := "INSERT INTO competition_lists " +
			"(user_id, center_id, test_date_time, ustudy_variant_id, language_id) " +
			"VALUES ($1,$2,$3,$4,$5)"

		stmt, err := database.Db.Prepare(statement)

		if err != nil {
			return err
		}

		_, err = stmt.Exec(u.Id, uTestCompetitionRespData.CenterId, uTestCompetitionRespData.TestDateTime,
			uTestCompetitionRespData.VariantId, uTestCompetitionRespData.LanguageId)

		if err != nil {
			return err
		}

		return err

	} else {
		err = errors.New("error encountered when connecting to UTest API server")
		return err
	}

}

func (u *User) CompetitionOrder() (competitionOrder CompetitionOrder, err error) {
	statement := "SELECT center_id,test_date_time,ustudy_variant_id,language_id,created_at FROM competition_lists WHERE user_id=$1"
	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	err = stmt.QueryRow(u.Id).Scan(
		&competitionOrder.CenterId,
		&competitionOrder.TestDateTime,
		&competitionOrder.UStudyVariantId,
		&competitionOrder.LanguageId,
		&competitionOrder.CreatedAt,
	)

	if err != nil {
		return
	}

	return
}

func (u *User) UStudyBalance() (balance float64) {
	idn := u.Idn
	idnStr := strconv.Itoa(int(idn))
	getUserBalanceUrl := "grant/userBalance/"
	s := []string{env.UtestApi, getUserBalanceUrl, idnStr}
	apiStr := strings.Join(s, "")

	resp, err := http.Get(apiStr)

	if err != nil {
		return
	}

	if resp.StatusCode == http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return balance
		}

		err = json.Unmarshal(body, &balance)

		if err != nil {
			return balance
		}

	} else {
		err = errors.New("error encountered when connecting to UTest API server")
		return balance
	}

	return
}

func (u *User) GetByEmail(email string) (user User, err error) {

	statement := `SELECT * FROM users
					INNER JOIN user_info ON users.id = user_info.user_id
					WHERE users.email=$1 AND deleted_at ISNULL`

	stmt, err := database.Db.Prepare(statement)
	defer stmt.Close()

	if err != nil {
		return
	}

	var userInfoId uint32
	err = stmt.QueryRow(email).Scan(
		&user.Id,
		&user.Idn,
		&user.FirstName,
		&user.LastName,
		&user.Email,
		&user.Password,
		&user.CreatedAt,
		&user.UpdatedAt,
		&user.DeletedAt,
		&userInfoId,
		&user.Id,
		&user.UserInfo.MiddleName,
		&user.UserInfo.BirthDate,
		&user.UserInfo.Gender,
		&user.UserInfo.MobilePhone,
		&user.UserInfo.WorkPhone,
		&user.UserInfo.ExtPhoneNumber,
		&user.UserInfo.Address,
	)

	if err != nil {
		return
	}

	roles, err := user.GetRoles()

	if err != nil {
		return
	}

	user.Roles = roles

	return
}

func (u *User) CreateRecoverPasswordToken(email string) (err error) {

	user, err := userModel.GetByEmail(email)

	if err != nil {
		return err
	}

	userEmailVerificationTokenString := utils.RandStringRunes(70)

	tx, err := database.Db.Begin()

	statement := "INSERT INTO user_email_activation_tokens (user_id, token) VALUES ($1, $2)"
	stmt, err := tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = stmt.Exec(user.Id, userEmailVerificationTokenString)

	if err != nil {
		tx.Rollback()
		return err
	}

	recoverPasswordEmailUrl := fmt.Sprintf("%s/user/recover-password/%s", env.GrantUStudyKZURL, userEmailVerificationTokenString)
	err = mail.SendRecoverPasswordEmail(recoverPasswordEmailUrl, email)

	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()

	return
}

func (u *User) GetEmailForPasswordRecover(userEmailVerificationTokenString string) (email string, err error) {

	statement := `SELECT email FROM users
					INNER JOIN user_email_activation_tokens ON users.id = user_email_activation_tokens.user_id
					WHERE users.deleted_at ISNULL AND token=$1`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return email, err
	}

	err = stmt.QueryRow(userEmailVerificationTokenString).Scan(&email)

	if err != nil {
		return email, err
	}

	return
}

func (u *User) RecoverPassword(form forms.RecoverPasswordForm) (err error) {

	tx, err := database.Db.Begin()

	if err != nil {
		return err
	}

	userId, err := utils.VerifyEmailTokenString(form.UserEmailVerificationTokenString, tx)

	if err != nil {
		tx.Rollback()
		return err
	}

	bytePassword := []byte(form.Password)
	hashedPassword, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)

	if err != nil {
		tx.Rollback()
		return
	}

	statement := "UPDATE users SET password=$1 WHERE id=$2 AND deleted_at ISNULL"
	stmt, err := tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return
	}

	_, err = stmt.Exec(hashedPassword, userId)

	if err != nil {
		tx.Rollback()
		return
	}

	tx.Commit()

	return
}

func (u *User) GetAll() (users []User, err error) {

	statement := "SELECT id FROM users WHERE deleted_at ISNULL"
	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return users, err
	}

	rows, err := stmt.Query()

	if err != nil {
		return users, err
	}

	for rows.Next() {
		var userId uint32
		err = rows.Scan(&userId)

		if err != nil {
			return users, err
		}

		user, err := userModel.Get(userId)

		if err != nil {
			return users, err
		}

		users = append(users, user)
	}

	return
}

func (u *User) CompetitionLists() (competitionLists []CompetitionList, err error) {

	statement := "SELECT * FROM competition_lists"
	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return competitionLists, err
	}

	rows, err := stmt.Query()

	if err != nil {
		return competitionLists, err
	}

	for rows.Next() {
		var userId uint32
		var centerId uint8
		var testDateTime time.Time
		var uStudyVariantId uint32
		var languageId uint8
		var createdAt time.Time
		err = rows.Scan(
			&userId,
			&centerId,
			&testDateTime,
			&uStudyVariantId,
			&languageId,
			&createdAt,
		)

		if err != nil {
			return competitionLists, err
		}

		user, err := userModel.Get(userId)

		if err != nil {
			return competitionLists, err
		}

		competitionList := CompetitionList{
			User: user,
			CompetitionOrder: CompetitionOrder{
				CenterId:        centerId,
				TestDateTime:    testDateTime,
				UStudyVariantId: uStudyVariantId,
				LanguageId:      languageId,
				CreatedAt:       createdAt,
			},
		}

		competitionLists = append(competitionLists, competitionList)
	}

	return
}
