package models

import (
	"time"
	"grantUstudy/database"
)

type Order struct {
	Id uint32 `json:"id"`
	CreatedAt time.Time `json:"createdAt"`
}

func (o *Order) Get(id uint32) (order Order, err error) {
	statement := "SELECT id, created_at FROM orders WHERE id=$1"
	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return order, err
	}

	err = stmt.QueryRow(id).Scan(&order.Id, &order.CreatedAt)

	if err != nil {
		return order, err
	}

	return order, err
}