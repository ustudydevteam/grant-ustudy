package models

import (
	"time"
	"github.com/lib/pq"
	"grantUstudy/forms"
	"grantUstudy/database"
	"strings"
	"fmt"
	"encoding/json"
	"database/sql"
	"grantUstudy/env"
	"net/http"
	"io/ioutil"
	"errors"
)

type Test struct {
	Id               uint32      `json:"id"`
	Name             string      `json:"name"`
	Modules          []Module    `json:"modules"`
	SelectiveModules []Module    `json:"selectiveModules"`
	GradeNumbers     []int64     `json:"gradeNumbers"`
	Language         Language    `json:"language"`
	TestInfo         TestInfo    `json:"testInfo"`
	CreatedAt        time.Time   `json:"createdAt"`
	UpdatedAt        time.Time   `json:"updatedAt"`
	DeletedAt        pq.NullTime `json:"-"`
}

type TestInfo struct {
	Type                  string        `json:"type"`
	Price                 float64       `json:"price"`
	Duration              time.Duration `json:"duration"`
	SelectiveModulesCount uint8         `json:"sel_mod_count"`
}

type GeneratedTest struct {
	Test    Test              `json:"test"`
	Modules []GeneratedModule `json:"modules"`
}

type GeneratedModule struct {
	Id        uint32              `json:"id"`
	Name      string              `json:"name"`
	Position  uint8               `json:"position"`
	Questions []GeneratedQuestion `json:"questions"`
}

type GeneratedQuestion struct {
	UserVariantId uint32   `json:"userVariantId"`
	Question      Question `json:"question"`
}

type TestResult struct {
	Order         Order          `json:"order"`
	ModuleResults []ModuleResult `json:"moduleResults"`
}

type ModuleResult struct {
	Module           Module `json:"module"`
	Score            uint8  `json:"score"`
	TotalComposition uint8  `json:"totalComposition"`
}

type TestCenter struct {
	Tb9906_id         string `json:"tb9906_id"`
	Tb9906_tb0013_id  string `json:"tb9906_tb0013_id"`
	Tb9906_addr_ru    string `json:"tb9906_addr_ru"`
	Tb9906_addr_kz    string `json:"tb9906_addr_kz"`
	Tb9906_latitude   string `json:"tb9906_latitude"`
	Tb9906_longitude  string `json:"tb9906_longitude"`
	Tb9906_capacity   string `json:"tb9906_capacity"`
	Tb9906_tb0001_ids string `json:"tb9906_tb0001_ids"`
	Tb0013_tb0013_id  string `json:"tb0013_tb0013_id"`
	Tb0013_name_ru    string `json:"tb0013_name_ru"`
	Tb0013_name_kz    string `json:"tb0013_name_kz"`
}

var moduleModel = new(Module)
var orderModel = new(Order)

func (t *Test) Create(form forms.TestCreateForm) (test Test, err error) {

	tx, err := database.Db.Begin()

	if err != nil {
		tx.Rollback()
		return
	}

	statement := "INSERT INTO tests (name, module_ids, grade_numbers, language_id) VALUES ($1,$2,$3,$4) " +
		"RETURNING id, name, created_at, updated_at"

	stmt, err := tx.Prepare(statement)
	defer stmt.Close()
	if err != nil {
		tx.Rollback()
		return
	}

	// check if the modules exist with channels
	for _, val := range form.ModuleIds {
		errCh := make(chan error)
		go func() {
			module, err := moduleModel.Get(val)
			errCh <- err
			test.Modules = append(test.Modules, module)
		}()

		err = <-errCh

		if err != nil {
			tx.Rollback()
			return test, err
		}

		close(errCh)
	}

	language, err := languageModel.Get(form.LanguageId)

	if err != nil {
		tx.Rollback()
		return test, err
	}

	test.Language = language

	// convert module ids to a string
	moduleIdsStr := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(form.ModuleIds)), ","), "[]")

	// convert grade numbers to a string
	gradeNumbersStr := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(form.GradeNumbers)), ","), "[]")

	err = stmt.QueryRow(form.Name, moduleIdsStr, gradeNumbersStr, form.LanguageId).Scan(
		&test.Id,
		&test.Name,
		&test.CreatedAt,
		&test.UpdatedAt,
	)

	if err != nil {
		tx.Rollback()
		return test, err
	}

	test.GradeNumbers = form.GradeNumbers

	tx.Commit()
	return
}

func (t *Test) Get(id uint32) (test Test, err error) {

	statement := "SELECT id,name,get_language(language_id),get_test_modules(id),get_test_selective_modules(id), get_test_info(id) FROM tests WHERE id=$1 AND deleted_at ISNULL"

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return
	}

	var languageStr string
	var modulesStr string
	var selectiveModulesStr sql.NullString
	var testInfoStr string

	var language Language
	var modules []Module
	var selectiveModules []Module
	var testInfo TestInfo
	err = stmt.QueryRow(id).Scan(
		&test.Id,
		&test.Name,
		&languageStr,
		&modulesStr,
		&selectiveModulesStr,
		&testInfoStr,
	)

	err = json.Unmarshal([]byte(languageStr), &language)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(modulesStr), &modules)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(testInfoStr), &testInfo)
	if err != nil {
		return
	}

	test.Language = language
	test.TestInfo = testInfo
	test.Modules = modules

	if test.TestInfo.SelectiveModulesCount > 0 {
		err = json.Unmarshal([]byte(selectiveModulesStr.String), &selectiveModules)
		if err != nil {
			return
		}
		test.SelectiveModules = selectiveModules
	}

	return
}

func (t *Test) GenerateResult(form forms.UserAnswerForm) (result TestResult, err error) {
	tx, err := database.Db.Begin()
	if err != nil {
		tx.Rollback()
		return result, err
	}

	for _, userAnswer := range form.UserAnswers {
		for _, answerId := range userAnswer.AnswerIds {
			statement := "INSERT INTO user_answers (user_variant_id, answer_id) VALUES ($1,$2) RETURNING id"
			stmt, err := tx.Prepare(statement)

			if err != nil {
				tx.Rollback()
				return result, err
			}

			var id uint32
			err = stmt.QueryRow(userAnswer.UserVariantId, answerId).Scan(&id)

			if err != nil {
				tx.Rollback()
				return result, err
			}
		}
	}

	tx.Commit()
	result, err = t.GetResult(form.OrderId)

	if err != nil {
		tx.Rollback()
		return result, err
	}

	return
}

func (t *Test) GetResult(orderId uint32) (result TestResult, err error) {

	statement := `SELECT modules.id, modules.name, sum(T1.correct_score_count) correct_scores_count, sum(T1.total_score_count) total_scores_count FROM
				  (
					SELECT
				  user_variants.id,
					  user_variants.module_id,
				  (
					SELECT sum(variant_scores.correct_score_count)
					FROM variant_scores(user_variants.id)
				  ) correct_score_count,
				  (
					SELECT sum(variant_scores.total_score_count)
					FROM variant_scores(user_variants.id)
				  ) total_score_count
				FROM user_variants
				WHERE user_variants.order_id = $1
				GROUP BY user_variants.id
				  ) T1
				INNER JOIN modules ON T1.module_id=modules.id
				GROUP BY modules.id`

	stmt, err := database.Db.Prepare(statement)

	if err != nil {
		return result, err
	}

	rows, err := stmt.Query(orderId)
	if err != nil {
		return result, err
	}

	for rows.Next() {
		var moduleResult ModuleResult
		err := rows.Scan(&moduleResult.Module.Id, &moduleResult.Module.Name, &moduleResult.Score, &moduleResult.TotalComposition)

		if err != nil {
			return result, err
		}
		result.ModuleResults = append(result.ModuleResults, moduleResult)
	}

	order, err := orderModel.Get(orderId)

	if err != nil {
		return result, err
	}

	result.Order = order

	return
}

func (t *Test) SelectedModules(selectedModuleIds []uint32) (selectedModules []Module, err error) {

	for _, val := range selectedModuleIds {
		selectedModule, err := moduleModel.Get(val)

		if err != nil {
			return selectedModules, err
		}

		selectedModules = append(selectedModules, selectedModule)
	}

	return
}

func (t *Test) Centers() (testCenters []TestCenter, err error) {
	getCentersUrl := "grant/centers"
	s := []string{env.UtestApi, getCentersUrl}
	apiStr := strings.Join(s, "")

	resp, err := http.Get(apiStr)

	if err != nil {
		return testCenters, err
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return testCenters, err
		}

		err = json.Unmarshal(body, &testCenters)

		if err != nil {
			return testCenters, err
		}

	} else {
		err = errors.New("error encountered when connecting to UTest API server")
		return testCenters, err
	}

	return
}
