package models

import (
	"time"
	"github.com/lib/pq"
)

type School struct {
	Id         uint32       `json:"id"`
	Bin        uint32       `json:"bin"`
	Phone1     string      `json:"phone1"`
	Phone2     string      `json:"phone2"`
	Email1     string      `json:"email1"`
	Email2     string      `json:"email2"`
	PostalCode string      `json:"postalCode"`
	Address    string      `json:"address"`
	CreatedAt  time.Time   `json:"createdAt"`
	UpdatedAt  time.Time   `json:"updatedAt"`
	DeletedAt  pq.NullTime `json:"deletedAt"`
}
