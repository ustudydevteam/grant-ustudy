package database

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"grantUstudy/env"
)

var Db *sql.DB

func init() {
	dbInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		env.DBHost,env.DBPort,env.DBUser,env.DBPassword,env.DBName)
	var err error
	Db, err = sql.Open("postgres", dbInfo)

	if err != nil {
		panic(err)
	}
}