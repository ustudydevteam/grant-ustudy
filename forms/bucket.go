package forms

type BucketCreateForm struct {
	Name        string `json:"name"`
	Location    string `json:"location"`
	Description string `json:"description"`
}
