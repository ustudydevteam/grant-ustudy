package forms

type SubjectTranslationForm struct {
	LanguageId uint8  `json:"languageId"`
	Name       string `json:"name"`
}

type SubjectCreateForm struct {
	LanguageIds  []uint8                  `json:"languageIds" binding:"required"`
	Translations []SubjectTranslationForm `json:"translations" binding:"required"`
}
