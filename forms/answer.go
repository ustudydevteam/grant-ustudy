package forms

type AnswerCreateForm struct {
	QuestionId uint32  `json:"questionId"`
	Body       string `json:"body" binding:"required"`
	IsCorrect  bool   `json:"isCorrect"`
}

type AnswerUpdateForm struct {
	Id         uint32  `json:"id"`
	QuestionId uint32  `json:"questionId"`
	Body       string `json:"body" binding:"required"`
	IsCorrect  bool   `json:"isCorrect"`
}
