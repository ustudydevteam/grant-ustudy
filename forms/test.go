package forms

type TestCreateForm struct {
	Name         string  `json:"name"`
	ModuleIds    []uint32 `json:"moduleIds"`
	GradeNumbers []int64 `json:"gradeNumbers"`
	LanguageId   uint8   `json:"languageId"`
}
