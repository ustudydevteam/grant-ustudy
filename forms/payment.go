package forms

type PaymentCreateForm struct {
	Id            uint32  `json:"id"`
	UserId        uint32  `json:"userId"`
	ProviderId    uint32  `json:"providerId"`
	TransactionId uint32  `json:"transactionId"`
	Amount        float64 `json:"amount"`
}
