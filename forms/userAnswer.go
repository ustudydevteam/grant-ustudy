package forms

type UserAnswerForm struct {
	OrderId     uint32                   `json:"orderId"`
	UserAnswers []UserVariantAnswersForm `json:"userAnswers"`
}

type UserVariantAnswersForm struct {
	UserVariantId uint32   `json:"userVariantId"`
	AnswerIds     []uint32 `json:"answerIds"`
}
