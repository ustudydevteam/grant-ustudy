package forms

type QuestionCreateForm struct {
	ParentId   uint32  `json:"parentId"`
	ThemeId    uint32 `json:"themeId" binding:"required"`
	TypeId     uint8  `json:"typeId" binding:"required"`
	LanguageId uint8  `json:"languageId" binding:"required"`
	Body       string `json:"body" binding:"required"`
	IsApproved bool   `json:"isApproved"`
}

type QuestionUpdateForm struct {
	ParentId   uint32  `json:"parentId"`
	ThemeId    uint32 `json:"themeId" binding:"required"`
	TypeId     uint8  `json:"typeId" binding:"required"`
	LanguageId uint8  `json:"languageId" binding:"required"`
	Body       string `json:"body" binding:"required"`
	IsApproved bool   `json:"isApproved"`
}
