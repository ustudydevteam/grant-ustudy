package forms

type ModuleCreateForm struct {
	IdName      string      `json:"idName"`
	Name        string      `json:"name"`
	Position    uint8       `json:"position"`
	Description string      `json:"description"`
}
