package forms

type ModuleChunkCreateForm struct {
	ModuleId        uint32   `json:"moduleId"`
	ThemeIds        []uint32 `json:"themeIds"`
	QuestionTypeIds []uint8 `json:"questionTypeIds"`
	Composition     uint8   `json:"composition"`
}
