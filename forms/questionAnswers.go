package forms

type QuestionAnswersCreateForm struct {
	Question QuestionCreateForm `json:"question"`
	Answers  []AnswerCreateForm `json:"answers"`
}

type QuestionAnswersUpdateForm struct {
	Question QuestionUpdateForm `json:"question"`
	Answers  []AnswerUpdateForm `json:"answers"`
}
