package forms

import "time"

type UserLoginForm struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

type UserCreateForm struct {
	Role      RoleForm `json:"role"`
	Idn       uint64   `json:"idn" binding:"required"`
	FirstName string   `json:"firstName"`
	LastName  string   `json:"lastName"`
	Email     string   `json:"email" binding:"required,email"`
	Password  string   `json:"password" binding:"required"`
	MobilePhone uint64 `json:"mobilePhone"`
}

type RoleForm struct {
	Id  uint8  `json:"id" binding:"required"`
	Key string `json:"key" binding:"required"`
}

type UserOrderTest struct {
	TestId             uint32   `json:"testId"`
	SelectiveModuleIds []uint32 `json:"selectiveModuleIds"`
}

type UserCompeteForm struct {
	Idn          uint64    `json:"idn"`
	LanguageId   uint8    `json:"languageId"`
	CenterId     uint8    `json:"centerId"`
	TestDateTime time.Time `json:"testDateTime"`
}

type RecoverPasswordForm struct {
	Password                         string `json:"password" binding:"required"`
	UserEmailVerificationTokenString string `json:"userEmailVerificationTokenString"`
}
