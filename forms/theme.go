package forms

type ThemeTranslationForm struct {
	LanguageId uint8  `json:"languageId"`
	Name       string `json:"name"`
}

type ThemeCreateForm struct {
	SubjectId    uint32                  `json:"subjectId"`
	GradeNumber  uint8                  `json:"gradeNumber"`
	Translations []ThemeTranslationForm `json:"translations"`
}

type ThemeUpdateForm struct {
	SubjectId    uint32                  `json:"subjectId"`
	GradeNumber  uint8                  `json:"gradeNumber"`
	Translations []ThemeTranslationForm `json:"translations"`
}

type ThemeFilterForm struct {
	SubjectId    uint32                  `json:"subjectId"`
	GradeNumber  uint8                  `json:"gradeNumber"`
} 


