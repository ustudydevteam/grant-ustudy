package forms

type ModuleWithChunksCreateForm struct {
	Module ModuleCreateForm             `json:"module"`
	Chunks []ModuleChunkCreateForm `json:"chunks"`
}
