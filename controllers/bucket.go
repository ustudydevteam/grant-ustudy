package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"grantUstudy/forms"
	"net/http"
)

type BucketController struct {}

var bucketModel = new(models.Bucket)

func (ctrl BucketController) Create(c *gin.Context) {
	var bucketCreateForm forms.BucketCreateForm

	err := c.BindJSON(&bucketCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	bucket, err := bucketModel.Create(bucketCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"bucket":bucket})
}