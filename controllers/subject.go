package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"strconv"
	"net/http"
	"grantUstudy/forms"
)

type SubjectController struct {}

var subjectModel = new(models.Subject)

func (ctrl SubjectController) Get(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		subject, err := subjectModel.Get(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"subject":subject})
	}
}

func (ctrl SubjectController) Create(c *gin.Context){
	var subjectCreateForm forms.SubjectCreateForm

	err := c.BindJSON(&subjectCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	subject, err := subjectModel.Create(subjectCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"subject":subject})
}

func (ctrl SubjectController) GetAll(c *gin.Context) {
	subjects, err := subjectModel.GetAll()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"subjects":subjects})
}

func (ctrl SubjectController) Delete(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		err := subjectModel.Delete(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message":"Предмет успешно удален"})
	}
}

func (ctrl SubjectController) GetThemes(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		themes, err := subjectModel.GetThemes(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"themes":themes})
	}
}

func (ctrl SubjectController) GetQuestionsCount(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		questionsCount, err := subjectModel.GetQuestionsCount(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"questionsCount":questionsCount})
	}
}

func (ctrl SubjectController) GetModulesCount(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		modulesCount, err := subjectModel.GetModulesCount(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"modulesCount":modulesCount})
	}
}