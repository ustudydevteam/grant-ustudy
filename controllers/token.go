package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
	"github.com/dgrijalva/jwt-go"
)

type TokenController struct {}

func (ctrl TokenController) Refresh(c *gin.Context) {
	err := c.BindJSON(&tokenModel)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	token, err := jwt.Parse(tokenModel.RefreshToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("ErrorWhileParsingToken")
		}
		return []byte(models.RefreshSecretKey), nil
	})

	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		userId := claims["userId"].(float64)
		userIdInt := uint32(userId)
		user, err := userModel.Get(userIdInt)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
			return
		}

		updatedToken, err := tokenModel.UpdateToken(user)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"token": updatedToken})

	} else {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":"err happened"})
		return
	}
}