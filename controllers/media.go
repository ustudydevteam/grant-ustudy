package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"strconv"
	"net/http"
	"path/filepath"
	"time"
	"math/rand"
)

type MediaController struct {}

var mediaModel = new(models.Media)

func (ctrl MediaController) QuestionAudioCreate(c * gin.Context) {
	id := c.PostForm("questionId")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		file, err := c.FormFile("audio")

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
			return
		}

		filePath, err := filepath.Abs(filepath.Join("static",".minio.sys","tmp",file.Filename))
		if err := c.SaveUploadedFile(file, filePath); err != nil {
			c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
			return
		}
		media, err := mediaModel.QuestionAudioCreate(id, filePath)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"media":media})

	}
}

func (ctrl MediaController) ImagesUpload(c *gin.Context) {
	rand.Seed(time.Now().UnixNano())
	file, err := c.FormFile("image")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	if err := c.SaveUploadedFile(file, "static/images/"+file.Filename); err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"location": "/static/images/"+file.Filename})
}

