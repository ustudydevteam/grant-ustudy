package controllers

import (
	"grantUstudy/models"
	"net/http"
	"github.com/gin-gonic/gin"
	"strconv"
)

type LanguageController struct {}

var languageModel = new(models.Language)

func (ctrl LanguageController) GetAll(c *gin.Context) {
	languages, err := languageModel.GetAll()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"languages":languages})
}

func (ctrl LanguageController) Get(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint8(i64)
		language, err := languageModel.Get(id)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"language":language})
	}
}