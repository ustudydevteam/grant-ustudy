package controllers

import (
	"grantUstudy/models"
	"net/http"
	"github.com/gin-gonic/gin"
	"grantUstudy/forms"
	"strconv"
)

type ThemeController struct {}

var themeModel = new(models.Theme)

func (ctrl ThemeController) Create(c *gin.Context){
	var themeCreateForm forms.ThemeCreateForm

	err := c.BindJSON(&themeCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	theme, err := themeModel.Create(themeCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"theme":theme})
}

func (ctrl ThemeController) Get(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		theme, err := themeModel.Get(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"theme":theme})
	}
}

func (ctrl ThemeController) Update(c *gin.Context) {

	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		var themeUpdateForm forms.ThemeUpdateForm

		err := c.BindJSON(&themeUpdateForm)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
			return
		}

		theme, err := themeModel.Update(id, themeUpdateForm)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"theme":theme})
	}

}

func (ctrl ThemeController) GetAll(c *gin.Context) {
	themes, err := themeModel.GetAll()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"themes":themes})
}

func (ctrl ThemeController) Delete(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		err := themeModel.Delete(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message":"Тема успешно удалена"})
	}
}

func (ctrl ThemeController) GetQuestions(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		questions, err := themeModel.GetQuestions(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"questions":questions})
	}
}

func (ctrl ThemeController) Filtered(c *gin.Context) {
	var themeFilterForm forms.ThemeFilterForm

	err := c.BindJSON(&themeFilterForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	themes, err := themeModel.Filtered(themeFilterForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"themes":themes})
}

func (ctrl ThemeController) GetQuestionsCount(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		questionsCount, err := themeModel.GetQuestionsCount(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"questionsCount":questionsCount})
	}
}

func (ctrl ThemeController) GetModulesCount(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		modulesCount, err := themeModel.GetModulesCount(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"modulesCount":modulesCount})
	}
}