package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type QuestionTypeController struct {}

var questionTypeModel = new(models.QuestionType)

func (ctrl QuestionTypeController) Get(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint8(i64)
		questionType, err := questionTypeModel.Get(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"questionType":questionType})
	}
}

func (ctrl QuestionTypeController) GetAll(c *gin.Context) {
	questionTypes, err := questionTypeModel.GetAll()

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"questionTypes":questionTypes})
}