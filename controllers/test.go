package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"grantUstudy/forms"
	"net/http"
	"strconv"
	"regexp"
)

type TestController struct {}

var testModel = new(models.Test)


func (ctrl TestController) Create(c *gin.Context) {
	var testCreateForm forms.TestCreateForm

	err := c.BindJSON(&testCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	test, err := testModel.Create(testCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"test":test.Name})
}

func (ctrl TestController) Get(c *gin.Context, id string) {
	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		test, err := testModel.Get(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"test":test})
	}
}

func (ctrl TestController) GenerateResult(c *gin.Context) {
	var userAnswerForm forms.UserAnswerForm

	err := c.BindJSON(&userAnswerForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	result, err := testModel.GenerateResult(userAnswerForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"result": result})
}

func (ctrl TestController) GetResult(c *gin.Context, orderId string) {
	if i64, err := strconv.ParseUint(orderId, 10,64); err == nil {
		id := uint32(i64)
		result, err := testModel.GetResult(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"result":result})
	}

}

func (ctrl TestController) Handler(c *gin.Context){
	path1 := c.Param("path1")
	path2 := c.Param("path2")

	re := regexp.MustCompile("[0-9]")
	param1 := []byte(path1)
	param2 := []byte(path2)
	isTestId := re.Match(param1)
	isOrderId := re.Match(param2)

	if isTestId && path2 == ""{
		id := path1
		ctrl.Get(c, id)
	}else if path1 == "result" && isOrderId {
		orderId := path2
		ctrl.GetResult(c, orderId)
	} else if path1 == "centers" && path2 == "" {
		ctrl.Centers(c)
	}else {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"err":"page not found"})
		return
	}
}

func (ctrl TestController) Centers(c *gin.Context) {
	testCenters, err := testModel.Centers()

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"testCenters":testCenters})
}

