package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"strconv"
	"net/http"
)

type ModuleController struct {}

var moduleModel = new(models.Module)

func (ctrl ModuleController) Get(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		module, err := moduleModel.Get(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"module":module})
	}
}

func (ctrl ModuleController) Chunks(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		moduleChunks, err := moduleModel.Chunks(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"moduleChunks":moduleChunks})
	}
}
