package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"strconv"
	"net/http"
)

type ModuleChunkController struct {}

var moduleChunkModel = new(models.ModuleChunk)

func (ctrl ModuleChunkController) Get(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		moduleChunk, err := moduleChunkModel.Get(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"moduleChunk":moduleChunk})
	}
}
