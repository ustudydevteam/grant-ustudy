package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

type GradeController struct {}

var gradeModel = new(models.Grade)

func (ctrl GradeController) AvailableNumbers(c *gin.Context) {
	numbers, err := gradeModel.AvailableNumbers()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"numbers": numbers})
}