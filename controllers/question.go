package controllers

import (
	"grantUstudy/models"
	"github.com/gin-gonic/gin"
	"grantUstudy/forms"
	"net/http"
	"strconv"
)

type QuestionController struct {}

var questionModel = new(models.Question)

func (ctrl QuestionController) Create(c *gin.Context) {
	var questionAnswersCreateForm forms.QuestionAnswersCreateForm

	err := c.BindJSON(&questionAnswersCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	question, err := questionModel.Create(questionAnswersCreateForm.Question, questionAnswersCreateForm.Answers)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"question":question})
}

func (ctrl QuestionController) Get(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		question, err := questionModel.Get(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"question":question})
	}
}

func (ctrl QuestionController) GetAll(c *gin.Context) {
	questions, err := questionModel.GetAll()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"questions":questions})
}

func (ctrl QuestionController) Update(c *gin.Context) {

	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		var questionAnswersUpdateForm forms.QuestionAnswersUpdateForm

		err := c.BindJSON(&questionAnswersUpdateForm)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
			return
		}

		question, err := questionModel.Update(id, questionAnswersUpdateForm.Question, questionAnswersUpdateForm.Answers)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"question":question})
	}

}

func (ctrl QuestionController) Delete(c *gin.Context) {
	id := c.Param("id")

	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		err := questionModel.Delete(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message":"Вопрос успешно удален"})
	}
}
