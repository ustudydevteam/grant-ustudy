package controllers

import (
	"grantUstudy/models"
	"strconv"
	"net/http"
	"github.com/gin-gonic/gin"
	"grantUstudy/forms"
	"regexp"
	"grantUstudy/middlewares"
)

type UserController struct{}

var userModel = new(models.User)
var tokenModel = new(models.Token)

func (ctrl UserController) Get(c *gin.Context, id string) {
	if i64, err := strconv.ParseUint(id, 10,64); err == nil {
		id := uint32(i64)
		user, err := userModel.Get(id)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"user":user})

	} else {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}
}

func (ctrl UserController) GetByIdn(c *gin.Context, idn string) {
	if i64, err := strconv.ParseUint(idn, 10,64); err == nil {
		idn := uint64(i64)
		user, err := userModel.GetByIdn(idn)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"user":user})

	} else {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}
}

func (ctrl UserController) Login(c *gin.Context) {
	var userLoginForm forms.UserLoginForm

	err := c.BindJSON(&userLoginForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": err.Error()})
		return
	}

	user, err := userModel.Login(userLoginForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": err.Error()})
		return
	}

	token, err := tokenModel.Create(user)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"token":token, "user": user})

}

func (ctrl UserController) Create(c *gin.Context) {
	var userCreateForm forms.UserCreateForm

	err := c.BindJSON(&userCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": err.Error()})
		return
	}

	user, err := userModel.Create(userCreateForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"user":user})
}

func (ctrl UserController) Handler(c *gin.Context){
	path1 := c.Param("path1")
	path2 := c.Param("path2")

	re := regexp.MustCompile("[0-9]")
	param1 := []byte(path1)
	param2 := []byte(path2)
	isId := re.Match(param1)
	isIdn := re.Match(param2)

	if isId && path2 == ""{
		id := path1
		ctrl.Get(c, id)
	}else if path1 == "getByIdn" && isIdn {
		idn := path2
		ctrl.GetByIdn(c, idn)
	} else if path1 == "orders" && path2 == "" {
		ctrl.Orders(c)
	} else if path1 == "competitionOrder" && path2 == "" {
		ctrl.CompetitionOrder(c)
	}else if path1 == "UStudyBalance" && path2 == "" {
		ctrl.UStudyBalance(c)
	} else if path1 == "createRecoverPasswordToken" && path2 != "" {
		email := path2
		ctrl.CreateRecoverPasswordToken(c, email)
	} else if path1 == "getEmailForPasswordRecover"  && path2 != "" {
		token := path2
		ctrl.GetEmailForPasswordRecover(c, token)
	} else if path1 == "competitionLists"  && path2 == "" {
		ctrl.CompetitionLists(c)
	} else {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"err":"page not found"})
		return
	}
}

func (ctrl UserController) OrderTest(c *gin.Context){
	var userOrderTestForm forms.UserOrderTest
	user, err := middlewares.AuthUser(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	err = c.BindJSON(&userOrderTestForm)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	test, err := testModel.Get(userOrderTestForm.TestId)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	orderId, generatedTest, err := user.OrderTest(test, userOrderTestForm.SelectiveModuleIds)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"orderId": orderId, "generatedTest": generatedTest})



}

func (ctrl UserController) OrderTrialTestByUtest(c *gin.Context){
	var userOrderTestForm forms.UserOrderTest
	user, err := middlewares.AuthUser(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	err = c.BindJSON(&userOrderTestForm)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	test, err := testModel.Get(userOrderTestForm.TestId)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	token, err := middlewares.GetHeaderToken(c)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	orderId, generatedTest, err := user.OrderTrialTestByUtest(token, test, userOrderTestForm.SelectiveModuleIds)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"orderId": orderId, "generatedTest": generatedTest})



}

func (ctrl UserController) Orders(c *gin.Context) {
	user, err := middlewares.AuthUser(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	orders, err := user.Orders()

	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"orders": orders})
}

func (ctrl UserController) Compete(c *gin.Context) {
	var userCompeteForm forms.UserCompeteForm
	user, err := middlewares.AuthUser(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	err = c.BindJSON(&userCompeteForm)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	userCompeteForm.Idn = user.Idn
	err = user.Compete(userCompeteForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "you have successfully competed"})
}

func (ctrl UserController) CompetitionOrder (c *gin.Context){
	user, err := middlewares.AuthUser(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	competitionOrder, err := user.CompetitionOrder()

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error":err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"competitionOrder": competitionOrder})

}

func (ctrl UserController) UStudyBalance (c *gin.Context){
	user, err := middlewares.AuthUser(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error":err.Error()})
		return
	}

	balance := user.UStudyBalance()

	c.JSON(http.StatusOK, gin.H{"balance": balance})

}

func (ctrl UserController) RecoverPassword(c *gin.Context) {

	userEmailVerificationTokenString := c.Param("token")

	var recoverPasswordForm forms.RecoverPasswordForm

	err := c.BindJSON(&recoverPasswordForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": err.Error()})
		return
	}

	recoverPasswordForm.UserEmailVerificationTokenString = userEmailVerificationTokenString

	err = userModel.RecoverPassword(recoverPasswordForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true})
}

func (ctrl UserController) CreateRecoverPasswordToken(c *gin.Context, email string) {
	err := userModel.CreateRecoverPasswordToken(email)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true})
}

func (ctrl UserController) GetEmailForPasswordRecover(c *gin.Context, userEmailVerificationTokenString string) {
	email, err := userModel.GetEmailForPasswordRecover(userEmailVerificationTokenString)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"email": email})
}

func (ctrl UserController) GetAll(c *gin.Context)  {
	users, err := userModel.GetAll()

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"users": users})
}

func (ctrl UserController) CompetitionLists(c *gin.Context)  {
	competitionLists, err := userModel.CompetitionLists()

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"competitionLists": competitionLists})
}