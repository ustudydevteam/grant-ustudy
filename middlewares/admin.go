package middlewares

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Admin(c *gin.Context)  {
	user, err := AuthUser(c)

	if err != nil{
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err})
		return
	}

	if user.IsAdmin() {
		c.Next()
	} else {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": "only admin role can request this API"})
		return
	}
}