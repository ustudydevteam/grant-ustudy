package middlewares

import (
	"strings"
	"github.com/gin-gonic/gin"
	"github.com/dgrijalva/jwt-go"
	"grantUstudy/models"
	"net/http"
	"fmt"
	"errors"
)

func Auth(c *gin.Context) {
	if strings.Contains(c.Request.URL.Path, "/user/login") ||
		strings.Contains(c.Request.URL.Path, "/user/register") ||
		strings.Contains(c.Request.URL.Path, "/static/images") ||
		strings.Contains(c.Request.URL.Path, "/token/refresh") ||
		strings.Contains(c.Request.URL.Path, "/user/getByIdn") ||
		strings.Contains(c.Request.URL.Path, "/user/recoverPassword") ||
		strings.Contains(c.Request.URL.Path, "/user/createRecoverPasswordToken") ||
		strings.Contains(c.Request.URL.Path, "/user/getEmailForPasswordRecover"){
		return
	}

	tokenStr, err := GetHeaderToken(c)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "invalid token"})
		return
	}

	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("ErrorWhileParsingToken")
		}
		return []byte(models.AccessSecretKey), nil
	})

	switch err.(type) {

	case nil:

		if !token.Valid {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "invalid token"})
			return
		}

		if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			c.Next()
		} else {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "invalid token"})
			return
		}

	case *jwt.ValidationError:
		vErr := err.(*jwt.ValidationError)

		switch vErr.Errors {
		case jwt.ValidationErrorExpired:
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Token Expired, get a new one"})
			return
		default:
			c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": "Error while Parsing Token!"})
			return
		}

	default:
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"error": "Error while Parsing Token!"})
		return
	}

}

func GetHeaderToken(c *gin.Context) (tokenStr string, err error) {
	authHeader := c.GetHeader("Authorization")
	if authHeader == "" {
		err = errors.New("no auth header")
		return
	}
	tokenStrSlice := strings.Split(authHeader, " ")

	if len(tokenStrSlice) != 2 || tokenStrSlice[0] != "Bearer" {
		err = errors.New("invalid header")
		return
	}
	tokenStr = tokenStrSlice[1]
	return
}

func AuthUser(c *gin.Context) (user models.User, err error) {
	var userModel = new(models.User)
	tokenStr, err := GetHeaderToken(c)

	if err != nil {
		return user, err
	}

	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("ErrorWhileParsingToken")
		}
		return []byte(models.AccessSecretKey), nil
	})

	if err != nil {
		return user, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		userId := claims["userId"].(float64)
		userIdInt := uint32(userId)

		user, err := userModel.Get(userIdInt)
		if err != nil {
			return user, err
		}

		return user, err

	} else {
		err = errors.New("error encountered when defining user from token")
		return user, err
	}

	return
}
