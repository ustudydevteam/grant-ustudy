package main

import (
	"github.com/gin-gonic/gin"
	"grantUstudy/controllers"
	"grantUstudy/middlewares"
	"net/http"
)

func main() {
	r := gin.Default()
	r.Use(middlewares.CORSMiddleware)
	r.Use(middlewares.Auth)
	r.Static("static/images", "static/images")

	http.Handle("/static/images/", http.StripPrefix("/static/images/", http.FileServer(http.Dir("static/images"))))
	v1 := r.Group("/grant/v1")
	{
		/*** START QUESTION ***/
		question := new(controllers.QuestionController)

		v1.POST("/question", middlewares.Admin, question.Create)
		v1.GET("/question/:id", middlewares.Admin, question.Get)
		v1.GET("/question", middlewares.Admin, question.GetAll)
		v1.PUT("/question/:id", middlewares.Admin, question.Update)
		v1.DELETE("/question/:id", middlewares.Admin, question.Delete)
		/*** END QUESTION ***/

		/*** START QuestionType ***/
		questionType := new(controllers.QuestionTypeController)
		v1.GET("/questionType", middlewares.Admin, questionType.GetAll)
		v1.GET("/questionType/:id", middlewares.Admin, questionType.Get)
		/*** END QuestionType ***/

		/*** START SUBJECT ***/
		subject := new(controllers.SubjectController)

		v1.GET("/subject/:id", middlewares.Admin, subject.Get)
		v1.GET("/subject", middlewares.Admin, subject.GetAll)
		v1.POST("/subject", middlewares.Admin, subject.Create)
		v1.DELETE("/subject/:id", middlewares.Admin, subject.Delete)
		v1.GET("/subject/:id/themes", middlewares.Admin, subject.GetThemes)
		v1.GET("/subject/:id/questionsCount", middlewares.Admin, subject.GetQuestionsCount)
		v1.GET("/subject/:id/modulesCount", middlewares.Admin, subject.GetModulesCount)
		/*** END SUBJECT ***/

		/*** START THEME ***/
		theme := new(controllers.ThemeController)

		v1.GET("/theme/:id", middlewares.Admin, theme.Get)
		v1.GET("/theme", middlewares.Admin, theme.GetAll)
		v1.POST("/theme", middlewares.Admin, theme.Create)
		v1.PUT("/theme/:id", middlewares.Admin, theme.Update)
		v1.DELETE("/theme/:id", middlewares.Admin, theme.Delete)
		v1.GET("/theme/:id/questions", middlewares.Admin, theme.GetQuestions)
		v1.GET("/theme/:id/questionsCount", middlewares.Admin, theme.GetQuestionsCount)
		v1.GET("/theme/:id/modulesCount", middlewares.Admin, theme.GetModulesCount)
		v1.POST("/theme/filtered", middlewares.Admin, theme.Filtered)
		/*** START THEME ***/

		/*** START BUCKET ***/
		bucket := new(controllers.BucketController)
		v1.POST("/bucket", bucket.Create)
		/*** END BUCKET ***/

		/*** START MEDIA ***/
		media := new(controllers.MediaController)
		v1.POST("/question/audio", middlewares.Admin, media.QuestionAudioCreate)
		v1.POST("/media/images", media.ImagesUpload)
		/*** END MEDIA ***/

		/*** START LANGUAGE ***/
		language := new(controllers.LanguageController)

		v1.GET("/language", middlewares.Admin, language.GetAll)
		v1.GET("/language/:id", language.Get)
		/*** END LANGUAGE ***/

		/*** START MODULE ***/
		module := new(controllers.ModuleController)

		v1.GET("/module/:id", module.Get)
		v1.GET("/module/:id/chunks", module.Chunks)
		/*** END MODULE ***/


		/*** START moduleChunk ***/
		moduleChunk := new(controllers.ModuleChunkController)

		v1.GET("/moduleChunk/:id", moduleChunk.Get)

		/*** END moduleChunk ***/

		/*** START TEST ***/
		test := new(controllers.TestController)

		v1.POST("/test", test.Create)
		v1.POST("/test/generateResult", test.GenerateResult)
		v1.GET("/test/:path1/:path2", test.Handler) //test/result/:orderId
		v1.GET("/test/:path1", test.Handler) //test/:id, //test/centers
		/*** END TEST ***/

		/*** START GRADE ***/
		grade := new(controllers.GradeController)

		v1.GET("/grade/availableNumbers", grade.AvailableNumbers)
		/*** END GRADE ***/


		/*** START USER ***/
		user := new(controllers.UserController)

		v1.GET("/user", middlewares.Admin, user.GetAll)
		v1.GET("/user/:path1/:path2", user.Handler) // /user/getByIdn/avatar
		v1.GET("/user/:path1", user.Handler) // /user/:id

		v1.POST("/user/login", user.Login)
		v1.POST("/user/register", user.Create)
		v1.POST("/user/orderTest", user.OrderTest)
		v1.POST("/user/orderTrialTestByUtest", user.OrderTrialTestByUtest)
		v1.POST("/user/compete", user.Compete)
		v1.POST("/user/recoverPassword/:token", user.RecoverPassword)
		/*** END USER ***/

		/*** START TOKEN ***/
		token := new(controllers.TokenController)
		v1.POST("/token/refresh", token.Refresh)
		/*** END TOKEN ***/
		
	}

	r.Run(":8080")
}
