package utils

import (
	"math/rand"
	"time"
	"database/sql"
)

func RandStringRunes(n uint8) string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func VerifyEmailTokenString(EmailVerificationTokenString string, tx *sql.Tx) (userId uint64, err error) {


	statement := "SELECT user_id FROM user_email_activation_tokens WHERE token=$1"
	stmt, err := tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return userId, err
	}

	err = stmt.QueryRow(EmailVerificationTokenString).Scan(&userId)

	if err != nil {
		tx.Rollback()
		return userId, err
	}

	statement = "UPDATE users SET deleted_at=NULL WHERE id=$1"
	stmt, err = tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return userId, err
	}

	_, err = stmt.Exec(userId)
	if err != nil {
		tx.Rollback()
		return userId, err
	}

	statement = "DELETE FROM user_email_activation_tokens WHERE token=$1"
	stmt, err = tx.Prepare(statement)

	if err != nil {
		tx.Rollback()
		return userId, err
	}

	_, err = stmt.Exec(EmailVerificationTokenString)

	if err != nil {
		tx.Rollback()
		return userId, err
	}

	return
}